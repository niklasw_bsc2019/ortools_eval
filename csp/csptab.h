/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file csptab.h
 * @author Niklas Wahrenberg
 */

#ifndef CSPTAB_H
#define CSPTAB_H

#include "ui_csptab.h"

#include <QTabWidget>

#include <atomic>

#include "csp/datamodel/cspentitylistmodel.h"
#include "csp/entities/abstractconstraint.h"
#include "csp/entities/constraintalldiff.h"
#include "csp/entities/constraintminmax.h"
#include "csp/entities/constraintminmaxeq.h"
#include "csp/entities/constraintmodproddiv.h"
#include "csp/entities/constraintscalarprod.h"
#include "csp/entities/constraintsimplerelation.h"
#include "csp/entities/constraintsum.h"
#include "csp/solver/cspsolverthread.h"
#include "util.h"



namespace Ui {
    class CspTab;
}



class CspTab : public QTabWidget
{
    Q_OBJECT

private:
    Ui::CspTab                                      *ui;
    CspEntityListModel<const Variable>              *varModel;
    CspEntityListModel<const AbstractConstraint>    *constrModel;
    CspSolverThread                                 *cspSolverThread;

    QStringListModel                                *comparatorStringListModel;

    QButtonGroup                                    *boxGroup1, *boxGroup2, *boxGroup3;
    QButtonGroup                                    *boxGroup4, *boxGroup5, *boxGroup6;

    QStringListModel                                *solutionModel;
    QVector<QPair<QString, QStringList>>            solutions;
    std::atomic<bool>                               *stopSolving;


    void initModels();
    void initButtonGroups();
    void initConnections();
    void initValidators();


public:
    explicit CspTab(QWidget *parent = nullptr);
    ~CspTab();

    Ui::CspTab*                                     getCspTabUI()       { return ui;            }
    CspEntityListModel<const Variable>*             getVarModel()       { return varModel;      }
    CspEntityListModel<const AbstractConstraint>*   getConstrModel()    { return constrModel;   }
    bool isSolverRunning()  { return cspSolverThread != nullptr; }

public slots:
    void setListSelectionMode(QAbstractItemView::SelectionMode mode);

    void abortSolving() { *stopSolving = true;  }

    void handleAddConstValueVar();
    void handleAddStartEndDomainVar();
    void handleAddListDomainVar();
    void handleVarDelete();
    void handleVarDeleteAll();

    void handleAddProdConstr();
    void handleAddModConstr();
    void handleAddDivConstr();
    void handleAddMinMaxConstr();
    void handleAddAllDiffConstr();
    void handleAddSimpleRelationConstr();
    void handleAddSumConstr();
    void handleAddMinMaxEqConstr();
    void handleAddScalarProdConstr();
    void handleConstrDelete();

    void handleSolve();
    void handleResultReceived(int solutionNr, const QString stats, const QStringList assignments);
    void handleSolutionClear();

    void generateNQueensProblem();
    void generateMagicSquareProblem();
    void generateLegsAndHeadsProblem();
    void generateMapColoringProblem();

    void handleActionExport();
    void handleActionImport();
};

#endif // CSPTAB_H
