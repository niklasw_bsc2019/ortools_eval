/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file constraintsimplerelation.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTSIMPLERELATION_H
#define CONSTRAINTSIMPLERELATION_H

#include "abstractconstraint.h"
#include "util.h"

class ConstraintSimpleRelation : public AbstractConstraint
{
    // var <relation> targetVar
    // or
    // var <relation> targetValue
private:
    const Variable* var;
          QString   relation;
          bool      targetsVar;
    const Variable* targetVar;
          int64     targetVarOffset;
          int64     targetValue;

public:

    // JSON ctor
    ConstraintSimpleRelation(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }


    ConstraintSimpleRelation(const Variable* var, const QString relation, const Variable* targetVar, int64 targetVarOffset = 0)
    :
    var(var), relation(relation), targetsVar(true), targetVar(targetVar), targetVarOffset(targetVarOffset)
    {
        initDisplayText();
    }


    ConstraintSimpleRelation(const Variable* var, const QString relation, int64 targetValue)
    :
    var(var), relation(relation), targetsVar(false), targetValue(targetValue)
    {
        initDisplayText();
    }


    void initDisplayText()
    {
        displayText = var->getName() + " " + relation + " " +
                      ( targetsVar ? targetVar->getName() : QString::number(targetValue));

        if ( targetVarOffset != 0 )
            displayText += (targetVarOffset > 0 ? " + " : " - ") + QString::number(targetVarOffset > 0 ? targetVarOffset : targetVarOffset/-1);
    }


    virtual bool contains(const Variable* var) const override
    {
        int varId = var->getId();

        if ( this->var->getId() == varId )
            return true;

        if ( targetsVar && targetVar->getId() == varId )
            return true;

        return false;
    }


    virtual void parseToJson(QJsonObject& jsonObj) const override
    {
        jsonObj[JSON::CONSTR_TYPE]      = JSON::RELATION;
        jsonObj[JSON::CONSTR_VAR1]      = var->getId();
        jsonObj[JSON::CONSTR_BOOL]      = targetsVar;
        jsonObj[JSON::CONSTR_RELATION]  = relation;

        if ( targetsVar )
        {
            jsonObj[JSON::CONSTR_VAR2]  = targetVar->getId();
            jsonObj[JSON::CONSTR_INT]   = targetVarOffset;
        }
        else
            jsonObj[JSON::CONSTR_VALUE] = targetValue;
    }


    virtual void parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap) override
    {
        var         = varMap[jsonObj.value(JSON::CONSTR_VAR1).toInt()];
        targetsVar  =        jsonObj.value(JSON::CONSTR_BOOL).toBool();
        relation    =        jsonObj.value(JSON::CONSTR_RELATION).toString();

        if ( targetsVar )
        {
            targetVar       = varMap[jsonObj.value(JSON::CONSTR_VAR2).toInt()];
            targetVarOffset =        jsonObj.value(JSON::CONSTR_INT).toInt();
        }
        else
            targetValue = jsonObj.value(JSON::CONSTR_VALUE).toInt();

        initDisplayText();
    }


    virtual void parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const override
    {
        Util::addConstrFuncPtr func = Util::getMethodByCompareSymbol(relation);

        if ( func != nullptr )
        {
            if ( targetsVar )
            {
                if ( targetVarOffset == 0)
                    (modelBuilder.*func)(varMap[var->getId()], varMap[targetVar->getId()]);
                else
                {
                    QSharedPointer<LinearExpr> linExp = QSharedPointer<LinearExpr>(new LinearExpr(varMap[targetVar->getId()]));
                    linExp->AddConstant(targetVarOffset);
                    (modelBuilder.*func)(varMap[var->getId()], *linExp);
                }
            }
            else
                (modelBuilder.*func)(varMap[var->getId()], targetValue);
        }
    }
};

#endif // CONSTRAINTSIMPLERELATION_H
