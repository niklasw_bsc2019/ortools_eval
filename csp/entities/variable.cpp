/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

#include <QJsonDocument>
#include "variable.h"
#include "util.h"


int Variable::idGenerator = 0;





//////////////////////////////////////////////////////////////////////////////////////////////
///     HELPER METHODS
//////////////////////////////////////////////////////////////////////////////////////////////

static constexpr QChar ELEMENT_OF_SYMBOL = 0x2208;

void buildVarDisplayText(QString& displayText, QString& name, QVector<int64>& domain, bool isListDomain)
{
    displayText.clear();
    displayText += name;
    displayText += " = x | x ";
    displayText += ELEMENT_OF_SYMBOL;
    displayText += " ";
    if ( isListDomain )
        displayText += Util::numberListAsString(domain);
    else
        displayText += "[" + QString::number(domain.first()) + " - " + QString::number(domain.last()) + "]";
}



void buildVarDisplayText(QString& displayText, QString& name, int64 value)
{
    displayText.clear();
    displayText += name;
    displayText += " = ";
    displayText += QString::number(value);
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     PRIVATE CTOR
//////////////////////////////////////////////////////////////////////////////////////////////

Variable::Variable(QString name) : CspEntityListItem ()
{
    id = Variable::idGenerator++;

    if ( name.length() == 0 )
        this->name = QObject::tr("unnamed") += QString::number(id);
    else
        this->name = name;
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     PUBLIC CTORs
//////////////////////////////////////////////////////////////////////////////////////////////

Variable::Variable(int64 value, QString name) : Variable(name)
{
    this->value = value;
    type        = CONST_VAR;
    buildVarDisplayText(displayText, this->name, value);
}



Variable::Variable(int64 domainStart, int64 domainEnd, QString name) : Variable(name)
{
    if ( domainStart == domainEnd )
    {
        value   = domainStart;
        type    = CONST_VAR;
        buildVarDisplayText(displayText, this->name, value);
        QMessageBox::warning(nullptr, STRINGS::WARN_TITLE,
                             QObject::tr("Made this a const var because of start == end"));
        return;
    }

    if ( domainStart > domainEnd )
        Util::swapValues(domainStart, domainEnd);

    domain.append(domainStart);
    domain.append(domainEnd);

    type = START_END_VAR;
    buildVarDisplayText(displayText, this->name, domain, false);
}


Variable::Variable(QString domainList, bool sort, QString name) : Variable(name)
{
    for ( QString& str : domainList.split(QRegExp(",|;"), QString::SkipEmptyParts) )
        domain.append(str.toInt());

    type = LIST_VAR;

    if ( sort )
        std::sort(domain.begin(), domain.end());

    buildVarDisplayText(displayText, this->name, domain, true);
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     JSON EXPORT /IMPORT
//////////////////////////////////////////////////////////////////////////////////////////////

void Variable::parseToJson(QJsonObject& jsonObj) const
{
    jsonObj["id"]    = id;
    jsonObj["name"]  = name;
    jsonObj["type"]  = type;

    if ( type == CONST_VAR )
    {
        jsonObj["value"] = value;
    }
    else
    {
        QJsonArray domainArr;

        for ( int64 i : domain )
            domainArr.append(i);

        jsonObj["domain"] = domainArr;
    }
}



void Variable::parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
{
    id       =                   jsonObj.value("id").toInt();
    name     =                   jsonObj.value("name").toString();
    type     = static_cast<TYPE>(jsonObj.value("type").toInt());

    // avoid reaching an imported ID again,
    // by adding vars manually from UI after the import
    Variable::idGenerator = id + 1;

    if ( type == CONST_VAR )
    {
        value = jsonObj.value("value").toInt();
        buildVarDisplayText(displayText, this->name, value);
    }
    else
    {
        QJsonArray domainArr = jsonObj.value("domain").toArray();

        for ( QJsonValue i : domainArr )
            domain.append(i.toInt());

        buildVarDisplayText(displayText, this->name, domain, type == LIST_VAR);
    }

    varMap[id] = this;
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     MODEL PARSE
//////////////////////////////////////////////////////////////////////////////////////////////

void Variable::parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap)  const
{
    IntVar modelVar; // domains are passed by reference!

    if ( type == CONST_VAR )
        modelVar = modelBuilder.NewConstant(value).WithName(name.toStdString());

    else if ( type == LIST_VAR)
    {
        QSharedPointer<Domain> dom  = QSharedPointer<Domain>(new Domain());
        *dom                        = Domain::FromValues(domain.toStdVector());
        modelVar                    = modelBuilder.NewIntVar(*dom).WithName(name.toStdString());
    }

    else if ( type == START_END_VAR )
    {
        QSharedPointer<Domain> dom  = QSharedPointer<Domain>(new Domain(domain.first(), domain.last()));
        modelVar                    = modelBuilder.NewIntVar(*dom).WithName(name.toStdString());
    }

    varMap.insert(id, modelVar);
}
