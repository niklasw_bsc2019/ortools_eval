/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file constraintminmax.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTMINMAX_H
#define CONSTRAINTMINMAX_H

#include "abstractconstraint.h"
#include "util.h"

class ConstraintMinMax : public AbstractConstraint
{
private:
    const Variable* var;
          bool      minimize;

public:

    // JSON ctor
    ConstraintMinMax(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }


    ConstraintMinMax(const Variable* var, bool minimize)
    :
    var(var), minimize(minimize)
    {
        initDisplayText();
    }


    void initDisplayText()
    {
        displayText = (minimize ? "min(" : "max(") + var->getName() + ")";
    }


    virtual bool contains(const Variable* var) const override
    {
        return this->var->getId() == var->getId();
    }


    virtual void parseToJson(QJsonObject& jsonObj) const override
    {
        jsonObj[JSON::CONSTR_TYPE]  = JSON::MIN_MAX;
        jsonObj[JSON::CONSTR_VAR1]  = var->getId();
        jsonObj[JSON::CONSTR_BOOL]  = minimize;
    }


    virtual void parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap) override
    {
        var      = varMap[jsonObj.value(JSON::CONSTR_VAR1).toInt()   ];
        minimize =        jsonObj.value(JSON::CONSTR_BOOL).toBool();
        initDisplayText();
    }


    virtual void parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const override
    {
        IntVar modelVar = varMap[var->getId()];

        if ( minimize ) modelBuilder.Minimize(modelVar);
        else            modelBuilder.Maximize(modelVar);
    }
};

#endif // CONSTRAINTMINMAX_H
