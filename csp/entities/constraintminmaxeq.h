/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file constraintminmaxeq.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTMINMAXEQ_H
#define CONSTRAINTMINMAXEQ_H

#include "abstractconstraint.h"
#include "util.h"

class ConstraintMinMaxEq : public AbstractConstraint
{
    // target = min(vars)
    // or
    // target = max(vars)
private:
          QVector<const Variable*>  vars;
    const Variable*                 targetVar;
          bool                      minimize;

public:

    // JSON ctor
    ConstraintMinMaxEq(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }


    ConstraintMinMaxEq(const QVector<const Variable*> vars, const Variable* targetVar, bool minimize)
    :
    targetVar(targetVar), minimize(minimize)
    {
        this->vars = vars;
        initDisplayText();
    }


    ~ConstraintMinMaxEq() override { vars.clear(); }


    void initDisplayText()
    {
         displayText = targetVar->getName() + " = " + (minimize ? "min(" : "max(")
                       + varListToString(vars) + ")";
    }


    virtual bool contains(const Variable* var) const override
    {
        int varId = var->getId();

        if ( targetVar->getId() == varId )
            return true;

        for ( const Variable* tmpVar : vars )
            if ( tmpVar->getId() == varId )
                return true;

        return false;
    }


    virtual void parseToJson(QJsonObject& jsonObj) const override
    {
        jsonObj[JSON::CONSTR_TYPE]  = JSON::MIN_MAX_EQ;
        jsonObj[JSON::CONSTR_VAR1]  = targetVar->getId();
        jsonObj[JSON::CONSTR_BOOL]  = minimize;

        QJsonArray jsonVars;
        for ( const Variable* v : vars )
            jsonVars.append(v->getId());

        jsonObj[JSON::CONSTR_VAR_LIST] = jsonVars;
    }


    virtual void parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap) override
    {
        targetVar = varMap[jsonObj.value(JSON::CONSTR_VAR1).toInt()   ];
        minimize  =        jsonObj.value(JSON::CONSTR_BOOL).toBool();

        QJsonArray jsonVars = jsonObj.value(JSON::CONSTR_VAR_LIST).toArray();
        for ( QJsonValue i : jsonVars )
            vars.append(varMap[i.toInt()]);

        initDisplayText();
    }


    virtual void parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const override
    {
        QVector<IntVar> modelVars;

        for ( const Variable* var : vars )
            modelVars.push_back(varMap[var->getId()]);

        if ( minimize ) modelBuilder.AddMinEquality(varMap[targetVar->getId()], modelVars);
        else            modelBuilder.AddMaxEquality(varMap[targetVar->getId()], modelVars);
    }
};

#endif // CONSTRAINTMINMAXEQ_H
