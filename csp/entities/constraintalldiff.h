/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file constraintalldiff.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTALLDIFF_H
#define CONSTRAINTALLDIFF_H

#include "abstractconstraint.h"
#include "util.h"

class ConstraintAllDiff : public AbstractConstraint
{
    // [var1, var2, var3, ..., varN]
    // var1 != var2
    // var1 != var3
    // var2 != var3
    // ...
private:
    QVector<const Variable*> vars;

public:

    // JSON ctor
    ConstraintAllDiff(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }


    ConstraintAllDiff(const QVector<const Variable*> vars)
    {
        this->vars = vars;
        initDisplayText();
    }


    ~ConstraintAllDiff() override { vars.clear(); }


    void initDisplayText()
    {
        displayText = "allDiff(" + varListToString(vars) + ")";
    }


    virtual bool contains(const Variable* var) const override
    {
        int varId = var->getId();

        for ( const Variable* tmpVar : vars )
            if ( tmpVar->getId() == varId )
                return true;

        return false;
    }


    virtual void parseToJson(QJsonObject& jsonObj) const override
    {
        jsonObj[JSON::CONSTR_TYPE] = JSON::ALL_DIFF;

        QJsonArray jsonVars;
        for ( const Variable* v : vars )
            jsonVars.append(v->getId());

        jsonObj[JSON::CONSTR_VAR_LIST] = jsonVars;
    }


    virtual void parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap) override
    {
        QJsonArray jsonVars = jsonObj.value(JSON::CONSTR_VAR_LIST).toArray();
        for ( QJsonValue i : jsonVars )
            vars.append(varMap[i.toInt()]);

        initDisplayText();
    }


    virtual void parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const override
    {
        QVector<IntVar> modelVars;

        for ( const Variable* var : vars )
            modelVars.push_back(varMap[var->getId()]);

        modelBuilder.AddAllDifferent(modelVars);
    }
};

#endif // CONSTRAINTALLDIFF_H
