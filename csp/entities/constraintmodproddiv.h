/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file constraintmodproddiv.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTMODPROD_H
#define CONSTRAINTMODPROD_H

#include "abstractconstraint.h"
#include "util.h"

class ConstraintModProdDiv : public AbstractConstraint
{
public:
    enum TYPE
    {
        MOD     = 0,
        PROD,   // 1
        DIV     // 2
    };

    // var % mod = targetVar
    // or
    // var * mod = targetVar
private:
    const Variable*   firstVar;
    const Variable*   secondVar;
    const Variable*   targetVar;
          TYPE        type;

public:

    // JSON ctor
    ConstraintModProdDiv(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }


    ConstraintModProdDiv(const Variable* firstVar, const Variable* secondVar, const Variable* targetVar, TYPE type)
    :
    firstVar(firstVar), secondVar(secondVar), targetVar(targetVar), type(type)
    {
        initDisplayText();
    }


    void initDisplayText()
    {
        displayText = firstVar->getName();

        switch ( type )
        {
            case TYPE::MOD:  displayText += " % "; break;
            case TYPE::PROD: displayText += " * "; break;
            case TYPE::DIV:  displayText += " / "; break;
        }

        displayText += secondVar->getName() + " = " + targetVar->getName();
    }


    virtual bool contains(const Variable* var) const override
    {
        int varId = var->getId();

        if      ( firstVar->getId()  == varId ) return true;
        else if ( secondVar->getId() == varId ) return true;
        else if ( targetVar->getId() == varId ) return true;

        return false;
    }


    virtual void parseToJson(QJsonObject& jsonObj) const override
    {
        jsonObj[JSON::CONSTR_TYPE]  = JSON::MOD_PROD_DIV;
        jsonObj[JSON::CONSTR_VAR1]  = firstVar->getId();
        jsonObj[JSON::CONSTR_VAR2]  = secondVar->getId();
        jsonObj[JSON::CONSTR_VAR3]  = targetVar->getId();
        jsonObj[JSON::CONSTR_ENUM]  = type;
    }


    virtual void parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap) override
    {
        firstVar  = varMap[jsonObj.value(JSON::CONSTR_VAR1).toInt()   ];
        secondVar = varMap[jsonObj.value(JSON::CONSTR_VAR2).toInt()   ];
        targetVar = varMap[jsonObj.value(JSON::CONSTR_VAR3).toInt()   ];
        type      = static_cast<TYPE>(jsonObj.value(JSON::CONSTR_ENUM).toInt());
        initDisplayText();
    }


    virtual void parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const override
    {
        if      ( type == TYPE::MOD )
            modelBuilder.AddModuloEquality(varMap[targetVar->getId()], varMap[firstVar->getId()], varMap[secondVar->getId()]);

        else if ( type == TYPE::PROD )
            modelBuilder.AddProductEquality(varMap[targetVar->getId()], { varMap[firstVar->getId()], varMap[secondVar->getId()] } );

        else if ( type == TYPE::DIV )
            modelBuilder.AddDivisionEquality(varMap[targetVar->getId()], varMap[firstVar->getId()], varMap[secondVar->getId()]);
    }
};

#endif // CONSTRAINTMODPROD_H
