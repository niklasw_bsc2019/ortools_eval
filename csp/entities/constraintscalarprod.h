/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file constraintscalarprod.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTSCALARPROD_H
#define CONSTRAINTSCALARPROD_H

#include "abstractconstraint.h"
#include "util.h"

class ConstraintScalarProd : public AbstractConstraint
{
    // varVec1 =: [varN, ..., varM]
    // intVec2 =: [intN, ..., intM]
    //
    // varVec1 * intVec2  = varVec1N * intVec2N + ... + varVec1M * intVec2M   = targetVar
    // or
    // varVec1 * intVec2  = varVec1N * varVec2N + ... + varVec1M * intVec2M   = targetValue
private:
          QVector<const Variable*>  varVec1;
          QString                   relation;
          QVector<int64>            intVec2;
          bool                      targetsVar;
    const Variable*                 targetVar;
          int64                     targetValue;

public:

    // JSON ctor
    ConstraintScalarProd(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }


    ConstraintScalarProd(const QVector<const Variable*> varVec1, const QVector<int64> intVec2, const QString relation, const Variable* targetVar)
    :
    varVec1(varVec1), relation(relation), intVec2(intVec2), targetsVar(true), targetVar(targetVar)
    {
        initDisplayText();
    }


    ConstraintScalarProd(const QVector<const Variable*> varVec1, const QVector<int64> intVec2, const QString relation, int64 targetValue)
    :
    varVec1(varVec1), relation(relation), intVec2(intVec2), targetsVar(false), targetValue(targetValue)
    {
        initDisplayText();
    }


    ~ConstraintScalarProd() override
    {
        varVec1.clear();
        intVec2.clear();
    }


    void initDisplayText()
    {
        displayText = "[" + varListToString(varVec1)
                      + "] * "
                      + Util::numberListAsString(intVec2)
                      + " " + relation + " "
                      + (targetsVar ? targetVar->getName() : QString::number(targetValue));
    }


    virtual bool contains(const Variable* var) const override
    {
        int varId = var->getId();

        if ( targetsVar && targetVar->getId() == varId )
            return true;

        for ( const Variable* tmpVar : varVec1 )
            if ( tmpVar->getId() == varId )
                return true;

        return false;
    }


    virtual void parseToJson(QJsonObject& jsonObj) const override
    {
        jsonObj[JSON::CONSTR_TYPE]      = JSON::SCALAR;
        jsonObj[JSON::CONSTR_RELATION]  = relation;
        jsonObj[JSON::CONSTR_BOOL]      = targetsVar;

        QJsonArray jsonVars;
        for ( const Variable* v : varVec1 )
            jsonVars.append(v->getId());
        jsonObj[JSON::CONSTR_VAR_LIST] = jsonVars;

        QJsonArray jsonInts;
        for ( int64 i : intVec2 )
            jsonInts.append(i);
        jsonObj[JSON::CONSTR_INT_LIST] = jsonInts;

        if ( targetsVar )   jsonObj[JSON::CONSTR_VAR1]  = targetVar->getId();
        else                jsonObj[JSON::CONSTR_VALUE] = targetValue;
    }


    virtual void parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap) override
    {
        relation    = jsonObj.value(JSON::CONSTR_RELATION).toString();
        targetsVar  = jsonObj.value(JSON::CONSTR_BOOL).toBool();

        QJsonArray jsonVars = jsonObj.value(JSON::CONSTR_VAR_LIST).toArray();
        for ( QJsonValue i : jsonVars )
            varVec1.append(varMap[i.toInt()]);

        QJsonArray jsonInts = jsonObj.value(JSON::CONSTR_INT_LIST).toArray();
        for ( QJsonValue i : jsonInts )
            intVec2.append(i.toInt());

        if ( targetsVar )   targetVar   = varMap[jsonObj.value(JSON::CONSTR_VAR1).toInt()];
        else                targetValue =        jsonObj.value(JSON::CONSTR_VALUE).toInt();

        initDisplayText();
    }


    virtual void parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const override
    {
        QVector<IntVar> modelVars;
        for ( const Variable* var : varVec1 )
            modelVars.push_back(varMap[var->getId()]);

        Util::addConstrFuncPtr func = Util::getMethodByCompareSymbol(relation);

        if ( func != nullptr )
        {
            if ( targetsVar ) (modelBuilder.*func)(LinearExpr::ScalProd(modelVars, intVec2), varMap[targetVar->getId()]);
            else              (modelBuilder.*func)(LinearExpr::ScalProd(modelVars, intVec2), targetValue);
        }
    }
};

#endif // CONSTRAINTSCALARPROD_H
