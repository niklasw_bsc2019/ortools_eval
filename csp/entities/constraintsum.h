/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file constraintsum.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTSUM_H
#define CONSTRAINTSUM_H

#include "abstractconstraint.h"
#include "util.h"

class ConstraintSum : public AbstractConstraint
{
    // sum(var1, var2, var3, ..., varN) = targetVar
    // or
    // sum(var1, var2, var3, ..., varN) = targetValue
private:
          QVector<const Variable*>  vars;
          QString                   relation;
          bool                      targetsVar;
    const Variable*                 targetVar;
          int64                     targetValue;

public:

    // JSON ctor
    ConstraintSum(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }


    ConstraintSum(const QVector<const Variable*> vars, const QString relation, const Variable* targetVar)
    :
    relation(relation), targetsVar(true), targetVar(targetVar)
    {
        this->vars = vars;
        initDisplayText();
    }


    ConstraintSum(const QVector<const Variable*> vars, const QString relation, int64 targetValue)
    :
    relation(relation), targetsVar(false), targetValue(targetValue)
    {
        this->vars = vars;
        initDisplayText();
    }


    ~ConstraintSum() override { vars.clear(); }


    void initDisplayText()
    {
        displayText = "sum(" + varListToString(vars) + ") "
                      + relation + " "
                      + (targetsVar ? targetVar->getName() : QString::number(targetValue));
    }


    virtual bool contains(const Variable* var) const override
    {
        int varId = var->getId();

        if ( targetsVar && targetVar->getId() == varId )
            return true;

        for ( const Variable* tmpVar : vars )
            if ( tmpVar->getId() == varId )
                return true;

        return false;
    }


    virtual void parseToJson(QJsonObject& jsonObj) const override
    {
        jsonObj[JSON::CONSTR_TYPE]      = JSON::SUM;
        jsonObj[JSON::CONSTR_BOOL]      = targetsVar;
        jsonObj[JSON::CONSTR_RELATION]  = relation;

        QJsonArray jsonVars;
        for ( const Variable* v : vars )
            jsonVars.append(v->getId());

        jsonObj[JSON::CONSTR_VAR_LIST] = jsonVars;

        if ( targetsVar )   jsonObj[JSON::CONSTR_VAR1]  = targetVar->getId();
        else                jsonObj[JSON::CONSTR_VALUE] = targetValue;
    }


    virtual void parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap) override
    {
        targetsVar = jsonObj.value(JSON::CONSTR_BOOL).toBool();
        relation   = jsonObj.value(JSON::CONSTR_RELATION).toString();

        QJsonArray jsonVars = jsonObj.value(JSON::CONSTR_VAR_LIST).toArray();
        for ( QJsonValue i : jsonVars )
            vars.append(varMap[i.toInt()]);

        if ( targetsVar )   targetVar   = varMap[jsonObj.value(JSON::CONSTR_VAR1).toInt()];
        else                targetValue =        jsonObj.value(JSON::CONSTR_VALUE).toInt();

        initDisplayText();
    }


    virtual void parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const override
    {
        QVector<IntVar> modelVars;
        for ( const Variable* var : vars )
            modelVars.push_back(varMap[var->getId()]);

        Util::addConstrFuncPtr func = Util::getMethodByCompareSymbol(relation);

        if ( func != nullptr )
        {
            if ( targetsVar ) (modelBuilder.*func)(LinearExpr::Sum(modelVars), varMap[targetVar->getId()]);
            else              (modelBuilder.*func)(LinearExpr::Sum(modelVars), targetValue);
        }
    }
};

#endif // CONSTRAINTSUM_H
