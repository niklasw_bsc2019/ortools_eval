/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file abstractconstraint.h
 * @author Niklas Wahrenberg
 */

#ifndef CONSTRAINTTYPE_H
#define CONSTRAINTTYPE_H

#include "variable.h"
#include "csp/datamodel/cspentitylistitem.h"

class AbstractConstraint : public CspEntityListItem
{
public:

    virtual bool contains(const Variable* var) const = 0;

    // [var1,var2,...,varN] -> "var1Name,var2Name,..."
    QString varListToString(QVector<const Variable*> vars, int max = 8)
    {
        QString varString = "";
        for ( int i = 0; i < vars.size(); ++i )
        {
            const Variable* var = vars[i];

            if ( i < max )
            {
                varString += var->getName();
                if ( i < vars.size()-1 )
                    varString += ", ";
            }
            else if ( i == max )
            {
                varString += "...";
                break;
            }
        }
        return varString;
    }

};

#endif // CONSTRAINTTYPE_H
