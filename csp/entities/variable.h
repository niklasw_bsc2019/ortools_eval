/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file variable.h
 * @author Niklas Wahrenberg
 */

#ifndef VARIABLE_H
#define VARIABLE_H

#include <QtCore>
#include <QtWidgets>
#include "csp/datamodel/cspentitylistitem.h"
using namespace operations_research;
using namespace operations_research::sat;

class Variable : public CspEntityListItem
{
    typedef _int64 int64;

private:
    static int idGenerator;

public:
    static void resetIdGenerator() { idGenerator = 0; }



private:
    enum TYPE
    {
        CONST_VAR       = 0,
        START_END_VAR,  //1
        LIST_VAR        //2
    };

    Variable(const QString name);   // generates name if none given
    int             id;
    QString         name;
    TYPE            type;
    int64           value;
    QVector<int64>  domain;

public:
    Variable(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)
        { parseFromJson(jsonObj, varMap); }

    Variable(int64 value, QString name = "");
    Variable(QString domainList, bool sort, QString name ="");
    Variable(int64 domainStart, int64 domainEnd, QString name ="");

    virtual ~Variable() override { domain.clear(); }

    int                 getId()                                 const               { return id;        }
    QString             getName()                               const               { return name;      }
    int64               getValue()                              const               { return value;     }
    QVector<int64>&     getDomain()                                                 { return domain;    }

    bool                operator==(Variable* otherVar)          const               { return id == otherVar->id;    }
    bool                operator!=(Variable* otherVar)          const               { return !(this == otherVar);   }

    virtual void        parseToJson(QJsonObject& jsonObj)                                       const   override;
    virtual void        parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)         override;

    virtual void        parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const   override;
};

#endif // VARIABLE_H
