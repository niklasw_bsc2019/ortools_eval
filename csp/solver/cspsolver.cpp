/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

#include "cspsolverthread.h"
#include "ortools/sat/cp_model.h"
#include "ortools/sat/cp_model_checker.h"
#include "ortools/util/time_limit.h"

using namespace operations_research;
using namespace operations_research::sat;



void CspSolverThread::run()
{
    CpModelBuilder      modelBuilder;
    QMap<int, IntVar>   modelVarMap;

    for ( const Variable* var : *varModel )
        var->parseIntoModel(modelBuilder, modelVarMap);

    for ( const AbstractConstraint* constr : *constrModel )
        constr->parseIntoModel(modelBuilder, modelVarMap);


    SatParameters parameters;
    parameters.set_enumerate_all_solutions(allSolutions);


    int cntr = 0;
    Model model;
    model.Add(NewSatParameters(parameters));

    model.GetOrCreate<TimeLimit>()->RegisterExternalBooleanAsLimit(stopSolving);
    model.Add(NewFeasibleSolutionObserver([&](const CpSolverResponse& response) {   // solution printer callback
        QStringList assignments;
        for ( IntVar& modelVar : modelVarMap.values() )
        {
            QString tmp = QString::fromStdString(modelVar.Name());
            tmp += " = ";
            tmp += QString::number(SolutionIntegerValue(response, modelVar));
            assignments << tmp;
        }
        ++cntr;
        emit resultReady(cntr, QString::fromStdString(CpSolverResponseStats(response)), assignments);
     }));


    std::string errString = ValidateCpModel(modelBuilder.Proto());
    if ( !errString.empty() )
    {
        emit invalidModelDetected(QString::fromStdString(errString));
        return;
    }

    const CpSolverResponse response = SolveWithModel(modelBuilder, &model);

    if ( cntr == 0 )
        emit noSolutionFound(QString::fromStdString(CpSolverResponseStats(response)));
}
