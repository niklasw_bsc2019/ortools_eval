/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file cspsolverthread.h
 * @author Niklas Wahrenberg
 */

#ifndef CSPSOLVER_H
#define CSPSOLVER_H

#include <QObject>
#include <QThread>

#include "csp/datamodel/cspentitylistmodel.h"
#include "csp/entities/abstractconstraint.h"

class CspSolverThread : public QThread
{
    Q_OBJECT

private:
    CspEntityListModel<const Variable>*             varModel;
    CspEntityListModel<const AbstractConstraint>*   constrModel;
    bool                                            allSolutions;
    std::atomic<bool>*                              stopSolving;

public:
    CspSolverThread(CspEntityListModel<const Variable>* varModel, CspEntityListModel<const AbstractConstraint>* constrModel, bool allSolutions, std::atomic<bool>* stopSolving)
    :
    varModel(varModel), constrModel(constrModel), allSolutions(allSolutions), stopSolving(stopSolving)
    {}

    void run() override;

signals:
    void resultReady(int solutionNr, const QString stats, const QStringList assignments);
    void invalidModelDetected(const QString errMsg);
    void noSolutionFound(const QString msg);
};

#endif // CSPSOLVER_H
