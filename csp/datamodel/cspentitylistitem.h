/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file cspentitylistitem.h
 * @author Niklas Wahrenberg
 */

#ifndef CSPENTITYLISTITEM_H
#define CSPENTITYLISTITEM_H

#include <QString>
#include <QJsonObject>

#include "ortools/sat/cp_model.h"

//#include "csp/entities/variable.h"
// forward declaration instead of include to avoid
// "unterminated conditional directive" when 2 headers include each other
class Variable;
using namespace operations_research::sat;

class CspEntityListItem
{
protected:
    QString displayText;

public:    
                        CspEntityListItem() : displayText(""){}

            QString     getDisplayText() const { return displayText; }

    virtual             ~CspEntityListItem(){}

    virtual void        parseToJson(QJsonObject& jsonObj)                                       const   = 0;
    virtual void        parseFromJson(QJsonObject& jsonObj, QMap<int, const Variable*>& varMap)         = 0;

    virtual void        parseIntoModel(CpModelBuilder& modelBuilder, QMap<int, IntVar>& varMap) const   = 0;
};

#endif // CSPENTITYLISTITEM_H
