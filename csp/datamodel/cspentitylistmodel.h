/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file cspentitylistmodel.h
 * @author Niklas Wahrenberg
 */

#ifndef VARIABLELISTMODEL_H
#define VARIABLELISTMODEL_H

#include <QtCore>
#include <QAbstractListModel>
#include <type_traits>

#include "cspentitylistitem.h"
#include "csp/entities/variable.h"
#include "csp/entities/abstractconstraint.h"

#include "util.h"



class CspTab;



template<class T>
class CspEntityListModel : public QAbstractListModel
{
    // enforce: CspEntityListModel<T extends CspEntityListItem>
    static_assert(std::is_base_of<CspEntityListItem, T>::value, "T must inherit CspEntityListItem");

private:
    QVector<const T*> items;

public:

    // problem generator slots have direct access so that they can populate the items member
    // and update all connected views in one go via beginInsertRows() & endInsertRows()
    friend class CspTab;

    CspEntityListModel(QObject *parent = nullptr) : QAbstractListModel(parent) {}
    ~CspEntityListModel() override { deleteAll(); }

    // make model for each iterable
    typename QVector<const T*>::iterator begin()                { return items.begin();     }
    typename QVector<const T*>::const_iterator cbegin() const   { return items.cbegin();    }
    typename QVector<const T*>::iterator end()                  { return items.end();       }
    typename QVector<const T*>::const_iterator cend()   const   { return items.cend();      }

    int         count()                                 const   { return items.size();      }
    int         size()                                  const   { return items.size();      }
    bool        isEmpty()                               const   { return items.size() == 0; }
    bool        isNotEmpty()                            const   { return items.size() != 0; }

    void        addItem(const T* item);                                 // addItem(new v3): [*v1,*v2]     -> [*v1,*v2,*v3]
    void        deleteItem(int index);                                  // deleteItem(1):   [*v1,*v2,*v3] -> [*v1,*v3]
    void        deleteItem(const T* item) { items.removeAll(item); }    // deleteItem(*v1): [*v1,*v2,*v1] -> [*v2]
    void        deleteAll();                                            // deleteAll():     [*v1,*v2,*v3] -> []

    const T*    at(int index)          const { return items[index]; }
    const T*    operator[](int index)  const { return items[index]; }

    QVariant    headerData(int section, Qt::Orientation orientation, int role)  const override;
    QVariant    data(const QModelIndex &index, int role)                        const override;
    int         rowCount(const QModelIndex &parent = QModelIndex())             const override;
};



template<class T>
void CspEntityListModel<T>::addItem(const T* item)
{
    // https://doc.qt.io/qt-5/qabstractitemmodel.html#beginInsertRows
    // Alternatively, you can provide your own API for altering the data.
    // In either case, you will need to call beginInsertRows() and endInsertRows()
    // to notify other components that the model has changed.
    QAbstractItemModel::beginInsertRows(QModelIndex(), /*firstIndex=*/items.size(), /*lastIndex=*/items.size());
    items.append(item);
    QAbstractItemModel::endInsertRows();
}



template<class T>
void CspEntityListModel<T>::deleteItem(int index)
{
    if ( index != -1)
    {
        QAbstractItemModel::beginRemoveRows(QModelIndex(), index, index);
        delete items.at(index);
        items.remove(index);
        QAbstractItemModel::endRemoveRows();
    }
}



template<class T>
void CspEntityListModel<T>::deleteAll()
{
    if ( items.size() > 0 )
    {
        QAbstractItemModel::beginRemoveRows(QModelIndex(), 0, items.size()-1);
        qDeleteAll(items);  // call delete on each item
        items.clear();      // after deleting the items themselves, delete ptr to them in this container
        QAbstractItemModel::endRemoveRows();
    }
}



// https://doc.qt.io/qt-5/model-view-programming.html
// Well behaved models also implement headerData()
// to give tree and table views something to display in their headers.
template<class T>
QVariant CspEntityListModel<T>::headerData(int section, Qt::Orientation orientation,int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if      ( std::is_same<T, Variable>::value )                return tr("Variables");
    else if ( std::is_base_of<AbstractConstraint, T>::value )   return tr("Constraints");
    else                                                        return tr("Unknown CSP entity");
}



template<class T>
QVariant CspEntityListModel<T>::data(const QModelIndex &index, int role) const
{
    if ( !index.isValid() || index.row() >= items.size() )
        return QVariant();

    if ( role == Qt::DisplayRole )
        return items.at(index.row())->getDisplayText();

    if ( role == Qt::ToolTipRole )
    {
        const Variable* var = dynamic_cast<const Variable*>(items.at(index.row()));
        if ( var != nullptr )
            return QString("ID ") + QString::number(var->getId());
        else
            return QVariant();
    }
    else
        return QVariant();
}



// https://doc.qt.io/qt-5/qabstractlistmodel.html
// The columnCount() function is implemented for interoperability with all kinds of views,
// but by default informs views that the model contains only one column.
template<class T>
int CspEntityListModel<T>::rowCount(const QModelIndex &parent) const
{
    return items.size();
}



#endif // VARIABLELISTMODEL_H
