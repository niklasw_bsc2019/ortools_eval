/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

#include "csptab.h"



CspTab::CspTab(QWidget *parent)
    :
    QTabWidget(parent),
    ui(new Ui::CspTab),
    varModel(new CspEntityListModel<const Variable>(this)),
    constrModel(new CspEntityListModel<const AbstractConstraint>(this)),
    cspSolverThread(nullptr),
    comparatorStringListModel(new QStringListModel(this)),
    boxGroup1(new QButtonGroup(this)),
    boxGroup2(new QButtonGroup(this)),
    boxGroup3(new QButtonGroup(this)),
    boxGroup4(new QButtonGroup(this)),
    boxGroup5(new QButtonGroup(this)),
    boxGroup6(new QButtonGroup(this)),
    solutionModel(new QStringListModel(this)),
    stopSolving(new std::atomic<bool>(false))
{
    ui->setupUi(this);

    initModels();
    initButtonGroups();
    initConnections();
    initValidators();
}



CspTab::~CspTab()
{
    handleVarDeleteAll();
    delete ui;
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     UI SETUP
//////////////////////////////////////////////////////////////////////////////////////////////

void CspTab::initModels()
{
    ui->varList->setModel(varModel);
    ui->constrList->setModel(constrModel);
    ui->solutionList->setModel(solutionModel);

    ui->scalProdVec1List->setModel(varModel);
    ui->scalProdTargetBox->setModel(varModel);

    ui->minMaxTargetBox->setModel(varModel);

    ui->prodFac1Box->setModel(varModel);
    ui->prodFac2Box->setModel(varModel);
    ui->prodBox->setModel(varModel);

    ui->modVarBox->setModel(varModel);
    ui->modModBox->setModel(varModel);
    ui->modTargetBox->setModel(varModel);

    ui->divNomBox->setModel(varModel);
    ui->divDenomBox->setModel(varModel);
    ui->divBox->setModel(varModel);

    ui->rel1Box->setModel(varModel);
    ui->rel2Box->setModel(varModel);

    ui->allDiffList->setModel(varModel);

    ui->sumList->setModel(varModel);
    ui->sumTargetBox->setModel(varModel);

    ui->minMaxEqTargetBox->setModel(varModel);
    ui->minMaxEqList->setModel(varModel);



    QStringList comparators;

    comparators << RELATIONS::EQ;
    comparators << RELATIONS::NEQ;
    comparators << RELATIONS::GREATER_EQ;
    comparators << RELATIONS::GREATER;
    comparators << RELATIONS::SMALLER_EQ;
    comparators << RELATIONS::SMALLER;

    comparatorStringListModel->setStringList(comparators);

    ui->scalProdCompBox->setModel(comparatorStringListModel);
    ui->relCompBox->setModel(comparatorStringListModel);
    ui->sumCompBox->setModel(comparatorStringListModel);
}



void CspTab:: initButtonGroups()
{
    // "box-only" groups

    boxGroup1->addButton(ui->minMaxMinRb);
    boxGroup1->addButton(ui->minMaxMaxRb);
    ui->minMaxMinRb->setChecked(true);

    boxGroup2->addButton(ui->minMaxEqMinRb);
    boxGroup2->addButton(ui->minMaxEqMaxRb);
    ui->minMaxEqMinRb->setChecked(true);

    boxGroup3->addButton(ui->allSolutionsBox);
    boxGroup3->addButton(ui->firstFoundBox);
    ui->allSolutionsBox->setChecked(true);



    // box groups that en-/disable belonging components
    // (first passed box/widget-pair is default enabled)

    Util::initGroup(boxGroup4,  ui->scalProdTargetBoxRb, ui->scalProdTargetBox,
                                ui->scalProdTargetSpRb, ui->scalProdTargetSp);

    Util::initGroup(boxGroup5,  ui->relTargetSpRb, ui->relTargetSp,
                                ui->rel2BoxRb, ui->rel2Box, ui->relOffsetSp);

    Util::initGroup(boxGroup6,  ui->sumTargetSpRb, ui->sumTargetSp,
                                ui->sumTargetBoxRb, ui->sumTargetBox);
}



void CspTab::initConnections()
{
    // VAR EDIT

    connect(ui->addConstValueVarBtn,        &QPushButton::clicked,
            this,                           &CspTab::handleAddConstValueVar);
    connect(ui->addStartEndDomainVarBtn,    &QPushButton::clicked,
            this,                           &CspTab::handleAddStartEndDomainVar);
    connect(ui->addListDomainVarBtn,        &QPushButton::clicked,
            this,                           &CspTab::handleAddListDomainVar);
    connect(ui->delVarBtn,                  &QPushButton::clicked,
            this,                           &CspTab::handleVarDelete);
    connect(ui->delAllVarsBtn,              &QPushButton::clicked,
            this,                           &CspTab::handleVarDeleteAll);



    // CONSTR EDIT

    connect(ui->addProdConstrBtn,           &QPushButton::clicked,
            this,                           &CspTab::handleAddProdConstr);
    connect(ui->addModConstrBtn,            &QPushButton::clicked,
            this,                           &CspTab::handleAddModConstr);
    connect(ui->addDivConstrBtn,            &QPushButton::clicked,
            this,                           &CspTab::handleAddDivConstr);
    connect(ui->addMinMaxConstrBtn,         &QPushButton::clicked,
            this,                           &CspTab::handleAddMinMaxConstr);
    connect(ui->addAllDiffBtn,              &QPushButton::clicked,
            this,                           &CspTab::handleAddAllDiffConstr);
    connect(ui->addRelationConstrBtn,       &QPushButton::clicked,
            this,                           &CspTab::handleAddSimpleRelationConstr);
    connect(ui->addSumConstrBtn,            &QPushButton::clicked,
            this,                           &CspTab::handleAddSumConstr);
    connect(ui->addMinMaxEqConstrBtn,       &QPushButton::clicked,
            this,                           &CspTab::handleAddMinMaxEqConstr);
    connect(ui->addScalarProdConstrBtn,     &QPushButton::clicked,
            this,                           &CspTab::handleAddScalarProdConstr);
    connect(ui->delConstrBtn,               &QPushButton::clicked,
            this,                           &CspTab::handleConstrDelete);
    connect(ui->delAllConstrBtn,            &QPushButton::clicked,
            constrModel,                    &CspEntityListModel<const AbstractConstraint>::deleteAll);



    // SELECTION CLEARING

    connect(ui->clearVarSelection,                  &QPushButton::clicked,
            ui->varList->selectionModel(),          &QItemSelectionModel::clearSelection);

    connect(ui->clearConstrSelections,              &QPushButton::clicked,
            ui->constrList->selectionModel(),       &QItemSelectionModel::clearSelection);
    connect(ui->clearConstrSelections,              &QPushButton::clicked,
            ui->scalProdVec1List->selectionModel(), &QItemSelectionModel::clearSelection);
    connect(ui->clearConstrSelections,              &QPushButton::clicked,
            ui->sumList->selectionModel(),          &QItemSelectionModel::clearSelection);
    connect(ui->clearConstrSelections,              &QPushButton::clicked,
            ui->allDiffList->selectionModel(),      &QItemSelectionModel::clearSelection);
    connect(ui->clearConstrSelections,              &QPushButton::clicked,
            ui->minMaxEqList->selectionModel(),     &QItemSelectionModel::clearSelection);



    // CONSTR STACK WIDGET

    connect(ui->stackBox, qOverload<int>(&QComboBox::currentIndexChanged), [this](int newIndex){
        ui->stackedWidget->setCurrentIndex(newIndex);
        ui->stackPrev->setEnabled(newIndex > 0);
        ui->stackNext->setEnabled(newIndex < ui->stackedWidget->count()-1);
    });

    connect(ui->stackPrev, &QPushButton::clicked, [this](){
            ui->stackBox->setCurrentIndex(ui->stackedWidget->currentIndex() - 1);
    });

    connect(ui->stackNext, &QPushButton::clicked, [this](){
            ui->stackBox->setCurrentIndex(ui->stackedWidget->currentIndex() + 1);
    });



    // SOLVING

    connect(ui->solveBtn,                   &QPushButton::clicked,
            this,                           &CspTab::handleSolve);
    connect(ui->solveStopBtn,               &QPushButton::clicked,
            this,                           &CspTab::abortSolving);
    connect(ui->solutionClearBtn,           &QPushButton::clicked,
            this,                           &CspTab::handleSolutionClear);

    connect(ui->solutionSelectionList, &QListWidget::currentRowChanged, [this](int currentRow){
        if ( currentRow < solutions.size() )
        {
            ui->solutionStatsTxt->setPlainText(solutions[currentRow].first);
            solutionModel->setStringList(solutions[currentRow].second);
        }
    });
}


void CspTab::initValidators()
{
    // list of values separated by , or ; from -9999 to 9999
    QString regExp("((-[1-9]{1}[0-9]{0,3}|0|[1-9]{1}[0-9]{0,3})(,|;))+");
    ui->varDomainListTxt->setValidator(new QRegExpValidator(QRegExp(regExp),this));
    ui->scalProdVec2Txt->setValidator(new QRegExpValidator(QRegExp(regExp),this));
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     HELPER METHODS
//////////////////////////////////////////////////////////////////////////////////////////////

// "varName"     -> "varName2"
// "varName123"  -> "varName124"
void incrementNumberedName(QLineEdit* line, QString varName)
{
    line->setText(varName);

    QString lineStr = line->text();
    for ( int i = 0; i < lineStr.length(); ++i)
    {
        if ( lineStr[i].isDigit() )
        {
            QString nr(lineStr.right(lineStr.size()-i));
            QString incremented(QString::number(nr.toInt()+1));
            line->setText(lineStr.left(i)+incremented);
            return;
        }
    }

    line->setText(varName+QString::number(2));
}



static const int VAR_LIMIT = 500000;

bool varLimitReached(QWidget *parent, int modelSize)
{
    bool limitReached = modelSize > VAR_LIMIT;
    if ( limitReached )
        QMessageBox::critical(parent, STRINGS::ERR_TITLE, STRINGS::ERR_VAR_LIMIT);
    return limitReached;
}



bool isIndexInvalid(QWidget *parent, int index)
{
    bool isInvalidIndex = (index == -1);
    if ( isInvalidIndex )
        QMessageBox::critical(parent, STRINGS::ERR_TITLE, STRINGS::ERR_MISSING_INPUT);
    return isInvalidIndex;
}



bool areIndexesInvalid(QWidget *parent, int index, int index2, int index3)
{
    bool isInvalidIndex = (index == -1 || index2 == -1 || index3 == -1);
    if ( isInvalidIndex )
        QMessageBox::critical(parent, STRINGS::ERR_TITLE, STRINGS::ERR_MISSING_INPUT);
    return isInvalidIndex;
}



bool hasSelection(QWidget *parent, const QItemSelectionModel* selectionModel)
{
    bool hasSelection = selectionModel->hasSelection();
    if ( !hasSelection )
        QMessageBox::critical(parent, STRINGS::ERR_TITLE, STRINGS::ERR_MISSING_INPUT);
    return hasSelection;
}



bool hasSelectionAndSelectedEnough(QWidget *parent, const QItemSelectionModel* selectionModel)
{
    if ( !hasSelection(parent,selectionModel) )
        return false;

    bool hasNotSelectedEnough = selectionModel->selectedIndexes().count() == 1;
    if ( hasNotSelectedEnough )
        QMessageBox::critical(parent, STRINGS::ERR_TITLE, STRINGS::ERR_NOT_ENOUGH_SELECTED);
    return !hasNotSelectedEnough;
}



void collectVarsIntoVectorFromSelection(
    const CspEntityListModel<const Variable>& varModel, QVector<const Variable*>& vars, const QModelIndexList& selection
)
{
    for ( int i = 0; i < selection.size(); ++i )
        vars.append(varModel[selection[i].row()]);
}





void CspTab::setListSelectionMode(QAbstractItemView::SelectionMode mode)
{
    ui->varList->setSelectionMode(mode);
    ui->constrList->setSelectionMode(mode);
    ui->scalProdVec1List->setSelectionMode(mode);
    ui->sumList->setSelectionMode(mode);
    ui->allDiffList->setSelectionMode(mode);
    ui->minMaxEqList->setSelectionMode(mode);
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     VAR HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////

void CspTab::handleAddConstValueVar()
{
    if ( varLimitReached(this, varModel->rowCount()) )
        return;

    QString nameStr = ui->varNameTxt1->text();
    int     value   = ui->varValueSp->value();

    const Variable* var = new Variable(value, nameStr);
    varModel->addItem(var);
    incrementNumberedName(ui->varNameTxt1,var->getName());
    ui->varValueSp->setValue(value+1);
}



void CspTab::handleAddStartEndDomainVar()
{
    if ( varLimitReached(this, varModel->rowCount()) )
        return;

    QString nameStr    = ui->varNameTxt2->text();
    int64  domainStart = ui->varDomainStartSp->value();
    int64  domainEnd   = ui->varDomainEndSp->value();

    const Variable* var = new Variable(domainStart, domainEnd, nameStr);
    varModel->addItem(var);
    incrementNumberedName(ui->varNameTxt2,var->getName());
}



void CspTab::handleAddListDomainVar()
{
    if ( varLimitReached(this, varModel->rowCount()) )
        return;

    QString nameStr     = ui->varNameTxt3->text();
    QString domainList  = ui->varDomainListTxt->text();

    if ( domainList.length() == 0 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, STRINGS::ERR_DOMAINLIST_REQUIRED);
        return;
    }

    const Variable* var = new Variable(domainList, ui->sortBox->isChecked(), nameStr);
    varModel->addItem(var);
    incrementNumberedName(ui->varNameTxt3,var->getName());
}



void CspTab::handleVarDelete()
{
    QItemSelectionModel* selectionModel = ui->varList->selectionModel();

    if ( selectionModel->hasSelection() )
    {
        const QModelIndexList indexList = selectionModel->selectedIndexes();

        if (
            QMessageBox::No ==
            QMessageBox::warning(this,  STRINGS::WARN_TITLE,
                                        tr("Really delete %0 variables (& also all constraints that depend on them)?").arg(indexList.count()),
                                        QMessageBox::Yes, QMessageBox::No
                                )
        )
            return;

        bool emptyConstrModel = constrModel->isEmpty();

        for ( int i = indexList.size()-1; i >=0; --i )
        {
            const Variable* var = varModel->at(indexList[i].row());
            if ( !emptyConstrModel )
            {
                // also delete constraints that reference the deleted variable(s)
                for ( int i = constrModel->rowCount()-1; i >= 0; --i )
                    if ( constrModel->at(i)->contains(var) )
                        constrModel->deleteItem(i);
            }
            varModel->deleteItem(var);
        }

        selectionModel->reset();

        if ( varModel->rowCount() == 0 )
            Variable::resetIdGenerator();
    }
    else
        QMessageBox::critical(this, STRINGS::ERR_TITLE, STRINGS::ERR_NO_ITEM_SELECTED);
}



void CspTab::handleVarDeleteAll()
{
    constrModel->deleteAll();
    ui->varList->selectionModel()->reset();

    varModel->deleteAll();
    ui->constrList->selectionModel()->reset();

    Variable::resetIdGenerator();
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     CONSTRAINT HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////

void CspTab::handleAddProdConstr()
{
    int fac1Index = ui->prodFac1Box->currentIndex();
    int fac2Index = ui->prodFac2Box->currentIndex();
    int prodIndex = ui->prodBox->currentIndex();

    if ( areIndexesInvalid(this, fac1Index, fac2Index, prodIndex) )
        return;

    const AbstractConstraint* constr = new ConstraintModProdDiv(varModel->at(fac1Index),
                                                               varModel->at(fac2Index),
                                                               varModel->at(prodIndex),
                                                               ConstraintModProdDiv::TYPE::PROD);
    constrModel->addItem(constr);
}



void CspTab::handleAddModConstr()
{
    int varIndex     = ui->modVarBox->currentIndex();
    int modIndex     = ui->modModBox->currentIndex();
    int targetIndex  = ui->modTargetBox->currentIndex();

    if ( areIndexesInvalid(this, varIndex, modIndex, targetIndex) )
        return;

    const AbstractConstraint* constr = new ConstraintModProdDiv(varModel->at(varIndex),
                                                               varModel->at(modIndex),
                                                               varModel->at(targetIndex),
                                                               ConstraintModProdDiv::TYPE::MOD);
    constrModel->addItem(constr);
}



void CspTab::handleAddDivConstr()
{
    int nomIndex    = ui->divNomBox->currentIndex();
    int denomIndex  = ui->divDenomBox->currentIndex();
    int divIndex    = ui->divBox->currentIndex();

    if ( areIndexesInvalid(this, nomIndex, denomIndex, divIndex) )
        return;

    const AbstractConstraint* constr = new ConstraintModProdDiv(varModel->at(nomIndex),
                                                               varModel->at(denomIndex),
                                                               varModel->at(divIndex),
                                                               ConstraintModProdDiv::TYPE::DIV);
    constrModel->addItem(constr);
}



void CspTab::handleAddMinMaxConstr()
{
    int varIndex = ui->minMaxTargetBox->currentIndex();

    if ( isIndexInvalid(this, varIndex) )
        return;

    const AbstractConstraint* constr = new ConstraintMinMax(varModel->at(varIndex), ui->minMaxMinRb->isChecked());
    constrModel->addItem(constr);
}



void CspTab::handleAddAllDiffConstr()
{
    const QItemSelectionModel* selectionModel = ui->allDiffList->selectionModel();

    if ( !hasSelectionAndSelectedEnough(this, selectionModel) )
        return;

    QVector<const Variable*> vars;
    collectVarsIntoVectorFromSelection(*varModel, vars, selectionModel->selectedIndexes());

    const AbstractConstraint* constr = new ConstraintAllDiff(vars);
    constrModel->addItem(constr);
}



void CspTab::handleAddSimpleRelationConstr()
{
    int rel1Index = ui->rel1Box->currentIndex();

    if ( isIndexInvalid(this, rel1Index) )
        return;

    if ( ui->rel2BoxRb->isChecked() )
    {
        int rel2Index = ui->rel2Box->currentIndex();

        if ( isIndexInvalid(this, rel2Index) )
            return;

        const AbstractConstraint* constr = new ConstraintSimpleRelation(varModel->at(rel1Index),
                                                                      ui->relCompBox->currentText(),
                                                                      varModel->at(rel2Index),
                                                                      ui->relOffsetSp->value());
        constrModel->addItem(constr);
    }
    else if ( ui->relTargetSpRb->isChecked() )
    {
        const AbstractConstraint* constr = new ConstraintSimpleRelation(varModel->at(rel1Index),
                                                                      ui->relCompBox->currentText(),
                                                                      ui->relTargetSp->value());
        constrModel->addItem(constr);
    }
}



void CspTab::handleAddSumConstr()
{
    const QItemSelectionModel* selectionModel = ui->sumList->selectionModel();

    if ( !hasSelectionAndSelectedEnough(this, selectionModel) )
        return;

    if ( ui->sumTargetBoxRb->isChecked() )
    {
        int sumTargetIndex = ui->sumTargetBox->currentIndex();

        if ( isIndexInvalid(this, sumTargetIndex) )
            return;

        QVector<const Variable*> vars;
        collectVarsIntoVectorFromSelection(*varModel, vars, selectionModel->selectedIndexes());

        const AbstractConstraint* constr = new ConstraintSum(vars,
                                                           ui->sumCompBox->currentText(),
                                                           varModel->at(sumTargetIndex));
        constrModel->addItem(constr);
    }
    else if ( ui->sumTargetSpRb->isChecked() )
    {
        QVector<const Variable*> vars;
        collectVarsIntoVectorFromSelection(*varModel, vars, selectionModel->selectedIndexes());

        const AbstractConstraint* constr = new ConstraintSum(vars,
                                                           ui->sumCompBox->currentText(),
                                                           ui->sumTargetSp->value());
        constrModel->addItem(constr);
    }
}



void CspTab::handleAddMinMaxEqConstr()
{
    int minMaxEqTargetIndex = ui->minMaxEqTargetBox->currentIndex();

    if ( isIndexInvalid(this, minMaxEqTargetIndex) )
        return;

    const QItemSelectionModel* selectionModel = ui->minMaxEqList->selectionModel();

    if ( !hasSelectionAndSelectedEnough(this, selectionModel) )
        return;

    QVector<const Variable*> vars;
    collectVarsIntoVectorFromSelection(*varModel, vars, selectionModel->selectedIndexes());

    const AbstractConstraint* constr = new ConstraintMinMaxEq(vars,
                                                            varModel->at(minMaxEqTargetIndex),
                                                            ui->minMaxEqMinRb->isChecked());
    constrModel->addItem(constr);
}



void CspTab::handleAddScalarProdConstr()
{
    const QItemSelectionModel* vec1Selection = ui->scalProdVec1List->selectionModel();

    if ( !hasSelectionAndSelectedEnough(this, vec1Selection) )
        return;

    QString vec2Txt  = ui->scalProdVec2Txt->text();
    QStringList vec2 = vec2Txt.split(QRegExp("(,|;)"), QString::SkipEmptyParts);
    int vec2Size     = vec2.size();

    if ( vec2Txt.length() == 0 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, STRINGS::ERR_MISSING_INPUT);
        return;
    }
    else if ( vec2Size != vec1Selection->selectedIndexes().size() )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, STRINGS::ERR_VEC_SIZE_NOT_EQ);
        return;
    }


    QString compare = ui->scalProdCompBox->currentText();

    QVector<int64> intVec2;
    for ( QString& str : vec2 )
        intVec2.append(str.toInt());

    if ( ui->scalProdTargetBoxRb->isChecked() )
    {
        int targetIndex = ui->scalProdTargetBox->currentIndex();
        if ( isIndexInvalid(this, targetIndex) )
            return;

        QVector<const Variable*> vars;
        collectVarsIntoVectorFromSelection(*varModel, vars, vec1Selection->selectedIndexes());

        const AbstractConstraint* constr = new ConstraintScalarProd(vars,
                                                                  intVec2,
                                                                  compare,
                                                                  varModel->at(targetIndex));
        constrModel->addItem(constr);
    }
    else if ( ui->scalProdTargetSpRb->isChecked() )
    {
        QVector<const Variable*> vars;
        collectVarsIntoVectorFromSelection(*varModel, vars, vec1Selection->selectedIndexes());

        const AbstractConstraint* constr = new ConstraintScalarProd(vars,
                                                                  intVec2,
                                                                  compare,
                                                                  ui->scalProdTargetSp->value());
        constrModel->addItem(constr);
    }
}



void CspTab::handleConstrDelete()
{
    QItemSelectionModel* selectionModel = ui->constrList->selectionModel();

    if ( selectionModel->hasSelection() )
    {
        QModelIndexList indexList = selectionModel->selectedIndexes();
        for ( int i = indexList.size()-1; i >=0; --i )
            constrModel->deleteItem(indexList[i].row());

        selectionModel->reset();
    }
    else
        QMessageBox::critical(this, STRINGS::ERR_TITLE, STRINGS::ERR_NO_ITEM_SELECTED);
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     SOLVE HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////

void CspTab::handleSolve()
{
    if ( varModel->count() == 0 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, STRINGS::ERR_VARS_MUST_EXIST);
        return;
    }

    handleSolutionClear();
    *stopSolving = false;

    ui->solveBtn->setEnabled(false);
    ui->solveStopBtn->setEnabled(true);
    ui->solutionClearBtn->setEnabled(false);

    cspSolverThread = new CspSolverThread(varModel, constrModel, ui->allSolutionsBox->isChecked(), stopSolving);

    connect(cspSolverThread, &CspSolverThread::invalidModelDetected, ui->solutionStatsTxt,   &QPlainTextEdit::setPlainText);
    connect(cspSolverThread, &CspSolverThread::noSolutionFound,      ui->solutionStatsTxt,   &QPlainTextEdit::setPlainText);
    connect(cspSolverThread, &CspSolverThread::resultReady,          this,                   &CspTab::handleResultReceived);
    connect(cspSolverThread, &QThread::finished, [this](){
        ui->solveBtn->setEnabled(true);
        ui->solveStopBtn->setEnabled(false);
        ui->solutionClearBtn->setEnabled(true);
        cspSolverThread->deleteLater();
        cspSolverThread = nullptr;
    });

    cspSolverThread->start();
}



void CspTab::handleResultReceived(int solutionNr, const QString stats, const QStringList assignments)
{
    setTabText(2, tr("Solutions (") + QString::number(solutionNr) + ")");
    ui->solutionSelectionList->addItem(QString(tr("Solution Nr.") + QString::number(solutionNr)));
    QPair<QString, QStringList> pair(stats, assignments);
    solutions.append(pair);
}



void CspTab::handleSolutionClear()
{
    setTabText(2, tr("Solutions"));

    solutionModel->removeRows(0, solutionModel->rowCount());
    solutions.clear();

    ui->solutionSelectionList->selectionModel()->reset();
    ui->solutionSelectionList->clear();

    ui->solutionStatsTxt->clear();
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     PROBLEM GENERATION HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////

void CspTab::generateNQueensProblem()
{
    bool ok;
    int queensNr = QInputDialog::getInt(this, STRINGS::MSG_INPUT_REQUIRED, tr("Number of queens:"), 4, 1, 1000, 1, &ok);

    if ( !ok )
        return;



    handleVarDeleteAll();
    handleSolutionClear();
    QVector<const Variable*> queensVec;


    // ADD VARS

    varModel->beginInsertRows(QModelIndex(), 0, queensNr-1);
    for ( int i = 0; i < queensNr; ++i )
    {
        const Variable* queen = new Variable(0, queensNr-1, tr("queenRow") + QString::number(i));
        queensVec.append(queen);
        varModel->items.append(queen);
    }
    varModel->endInsertRows();

    constrModel->beginInsertRows(QModelIndex(), constrModel->size(), constrModel->size() + 1);
    constrModel->items.append(new ConstraintAllDiff(queensVec));
    constrModel->endInsertRows();


    // ADD CONSTRS

    for ( int i = 0; i < queensNr; ++i )
    {
        QVector<const Variable*> diag1;
        QVector<const Variable*> diag2;

        varModel->beginInsertRows(QModelIndex(), varModel->size(), varModel->size() + queensNr);
        constrModel->beginInsertRows(QModelIndex(), constrModel->size(), constrModel->size() + queensNr + 2);
        for ( int j = 0; j < queensNr; ++j )
        {
            const Variable* q1 = new Variable(0, 2*queensNr, QString("diag1_%0").arg(i));
            varModel->items.append(q1);
            diag1.append(q1);
            constrModel->items.append(new ConstraintSimpleRelation(q1, RELATIONS::EQ, varModel->at(j), j));

            const Variable* q2 = new Variable(-queensNr, queensNr, QString("diag2_%0").arg(i));
            varModel->items.append(q2);
            diag2.append(q2);
            constrModel->items.append(new ConstraintSimpleRelation(q2, RELATIONS::EQ, varModel->at(j), -j));
        }
        varModel->endInsertRows();

        constrModel->items.append(new ConstraintAllDiff(diag1));
        constrModel->items.append(new ConstraintAllDiff(diag2));

        constrModel->endInsertRows();
    }
}



void CspTab::generateMagicSquareProblem()
{
    bool ok;
    int width = QInputDialog::getInt(this, STRINGS::MSG_INPUT_REQUIRED, tr("Width of magic suqare:"), 3, 1, 5, 1, &ok);

    if ( !ok )
        return;



    handleVarDeleteAll();
    handleSolutionClear();
    QVector<const Variable*> fields;
    int totalFields     = width*width;
    int magicConstant   = width * (totalFields + 1) / 2;



    // ADD VARS

    varModel->beginInsertRows(QModelIndex(), 0, totalFields-1);
    for ( int row = 0; row < width; ++row )
    {
        for ( int col = 0; col < width; ++col )
        {
            const Variable* field = new Variable(1, totalFields, QString("field") + QString::number(row) + QString::number(col));
            fields.append(field);
            varModel->items.append(field);
        }
    }
    varModel->endInsertRows();



    // ADD CONSTRS

    constrModel->beginInsertRows(QModelIndex(), 0, 2 + 2*width);
    constrModel->items.append(new ConstraintAllDiff(fields));

    QVector<const Variable*> diag1Sum;
    QVector<const Variable*> diag2Sum;

    for ( int row = 0; row < width; ++row )
    {
        QVector<const Variable*> rowSum;
        QVector<const Variable*> colSum;

        for ( int col = 0; col < width; ++col )
        {
            rowSum.append( fields[row*width + col      ] );
            colSum.append( fields[row       + col*width] );

            if (       row == col      ) diag1Sum.append(fields[row*width + col]);
            if ( row + col == width-1  ) diag2Sum.append(fields[row*width + col]);  // no else! in squares with odd width the middle field is on both diagonals
        }

        constrModel->items.append(new ConstraintSum(rowSum, RELATIONS::EQ, magicConstant));
        constrModel->items.append(new ConstraintSum(colSum, RELATIONS::EQ, magicConstant));
    }
    constrModel->items.append(new ConstraintSum(diag1Sum, RELATIONS::EQ, magicConstant));
    constrModel->items.append(new ConstraintSum(diag2Sum, RELATIONS::EQ, magicConstant));
    constrModel->endInsertRows();
}



static const QString MSG_ITEM_CHICKENS      = QObject::tr("Chickens");
static const QString MSG_ITEM_TRIPODS       = QObject::tr("Tripods");
static const QString MSG_ITEM_COWS          = QObject::tr("Cows");
static const QString MSG_ITEM_SPIDERS       = QObject::tr("Spiders");

static const QString MSG_ITEM_COMBO_2_1     = QString(MSG_ITEM_CHICKENS + " & " + MSG_ITEM_TRIPODS);
static const QString MSG_ITEM_COMBO_2_2     = QString(MSG_ITEM_CHICKENS + " & " + MSG_ITEM_COWS);
static const QString MSG_ITEM_COMBO_2_3     = QString(MSG_ITEM_CHICKENS + " & " + MSG_ITEM_SPIDERS);
static const QString MSG_ITEM_COMBO_2_4     = QString(MSG_ITEM_TRIPODS + " & " + MSG_ITEM_COWS);
static const QString MSG_ITEM_COMBO_2_5     = QString(MSG_ITEM_TRIPODS + " & " + MSG_ITEM_SPIDERS);
static const QString MSG_ITEM_COMBO_2_6     = QString(MSG_ITEM_COWS + " & " + MSG_ITEM_SPIDERS);

static const QString MSG_ITEM_COMBO_3_1     = QString(MSG_ITEM_CHICKENS + " & " + MSG_ITEM_TRIPODS + " & " + MSG_ITEM_COWS);
static const QString MSG_ITEM_COMBO_3_2     = QString(MSG_ITEM_CHICKENS + " & " + MSG_ITEM_TRIPODS + " & " + MSG_ITEM_SPIDERS);
static const QString MSG_ITEM_COMBO_3_3     = QString(MSG_ITEM_CHICKENS + " & " + MSG_ITEM_COWS + " & " + MSG_ITEM_SPIDERS);
static const QString MSG_ITEM_COMBO_3_4     = QString(MSG_ITEM_TRIPODS + " & " + MSG_ITEM_COWS + " & " + MSG_ITEM_SPIDERS);

static const QString MSG_ITEM_COMBO_4       = QString(MSG_ITEM_CHICKENS + " & " + MSG_ITEM_TRIPODS + " & " + MSG_ITEM_COWS + " & " + MSG_ITEM_SPIDERS);

static const QMap<QString, QVector<int64>> LEG_COMBOS = {
    { MSG_ITEM_COMBO_2_1, {2,3} },
    { MSG_ITEM_COMBO_2_2, {2,4} },
    { MSG_ITEM_COMBO_2_3, {2,8} },
    { MSG_ITEM_COMBO_2_4, {3,4} },
    { MSG_ITEM_COMBO_2_5, {3,8} },
    { MSG_ITEM_COMBO_2_6, {4,8} },

    { MSG_ITEM_COMBO_3_1, {2,3,4} },
    { MSG_ITEM_COMBO_3_2, {2,3,8} },
    { MSG_ITEM_COMBO_3_3, {2,4,8} },
    { MSG_ITEM_COMBO_3_4, {3,4,8} },

    { MSG_ITEM_COMBO_4, {2,3,4,8} }
};

static const QMap<int64, QString> ENT_NAME_BY_LEGS = {
    { 2, MSG_ITEM_CHICKENS },
    { 3, MSG_ITEM_TRIPODS },
    { 4, MSG_ITEM_COWS },
    { 8, MSG_ITEM_SPIDERS },
};

void CspTab::generateLegsAndHeadsProblem()
{
    bool ok;

    // QMap<QString, QVector<int64>> LEG_COMBOS
    QString item = QInputDialog::getItem(this, STRINGS::MSG_INPUT_REQUIRED, tr("Type of entities:"), LEG_COMBOS.keys(), 0, false, &ok);
    if (!ok || item.isEmpty())
        return;

    int totalHeads = QInputDialog::getInt(this, STRINGS::MSG_INPUT_REQUIRED, tr("Number of heads:"), 50, 0, 999999, 1, &ok);
    if ( !ok )
        return;

    int totalLegs = QInputDialog::getInt(this, STRINGS::MSG_INPUT_REQUIRED, tr("Number of legs:"), 123, 0, 9999999, 1, &ok);
    if ( !ok )
        return;


    QVector<int64> legCountList = LEG_COMBOS[item];    // get number of legs per entity mapped by the selected string
    handleVarDeleteAll();
    handleSolutionClear();
    QVector<const Variable*> ents;



    // ADD VARS

    // the selected list item determines the count of different entity types
    varModel->beginInsertRows(QModelIndex(), 0, legCountList.size()-1);
    for ( int i = 0; i < legCountList.size(); ++i )
    {
        // entity names are mapped by their legCount: QMap<int64, QString> ENT_NAME_BY_LEGS
              QString   entName     = ENT_NAME_BY_LEGS[legCountList[i]];
        const Variable* entityCount = new Variable(0, totalHeads, QString(tr("countOf") + entName));

        ents.append(entityCount);
        varModel->items.append(entityCount);
    }
    varModel->endInsertRows();



    // ADD CONSTRS

    constrModel->beginInsertRows(QModelIndex(), 0, 1);
    constrModel->items.append(new ConstraintSum(ents, RELATIONS::EQ, totalHeads));
    constrModel->items.append(new ConstraintScalarProd(ents, legCountList, RELATIONS::EQ, totalLegs));
    constrModel->endInsertRows();
}



void CspTab::generateMapColoringProblem()
{
    handleVarDeleteAll();
    handleSolutionClear();

    bool ok;
    QString item = QInputDialog::getItem(this, STRINGS::MSG_INPUT_REQUIRED, tr("Choose country:"), {tr("Australia"), tr("Germany")}, 0, false, &ok);

    if (!ok || item.isEmpty())
        return;

    int colors = QInputDialog::getInt(this, STRINGS::MSG_INPUT_REQUIRED, tr("Number of colors:"), 3, 0, 100, 1, &ok);
    if ( !ok )
        return;

    if ( item == tr("Australia") )
    {
        // ADD VARS
        QVector<const Variable*> fields;
        varModel->beginInsertRows(QModelIndex(), 0, 6);
        Variable* wa    = new Variable(0, colors-1, tr("Western Australia"));
        Variable* nt    = new Variable(0, colors-1, tr("Northern Territory"));
        Variable* sa    = new Variable(0, colors-1, tr("South Australia"));
        Variable* q     = new Variable(0, colors-1, tr("Queensland"));
        Variable* nsw   = new Variable(0, colors-1, tr("New South Wales"));
        Variable* v     = new Variable(0, colors-1, tr("Victoria"));
        Variable* t     = new Variable(0, colors-1, tr("Tasmania"));
        varModel->items.append(wa);
        varModel->items.append(nt);
        varModel->items.append(sa);
        varModel->items.append(q);
        varModel->items.append(nsw);
        varModel->items.append(v);
        varModel->items.append(t);
        varModel->endInsertRows();

        // ADD CONSTRS
        constrModel->beginInsertRows(QModelIndex(), 0, 2);
        constrModel->items.append(new ConstraintSimpleRelation(sa, RELATIONS::NEQ, wa));
        constrModel->items.append(new ConstraintSimpleRelation(sa, RELATIONS::NEQ, nt));
        constrModel->items.append(new ConstraintSimpleRelation(sa, RELATIONS::NEQ, q));
        constrModel->items.append(new ConstraintSimpleRelation(sa, RELATIONS::NEQ, nsw));
        constrModel->items.append(new ConstraintSimpleRelation(sa, RELATIONS::NEQ, v));
        constrModel->items.append(new ConstraintSimpleRelation(wa, RELATIONS::NEQ, nt));
        constrModel->items.append(new ConstraintSimpleRelation(nt, RELATIONS::NEQ, q));
        constrModel->items.append(new ConstraintSimpleRelation(q, RELATIONS::NEQ, nsw));
        constrModel->items.append(new ConstraintSimpleRelation(nsw, RELATIONS::NEQ, v));
        constrModel->endInsertRows();
    }
    else if ( item == tr("Germany") )
    {
        // ADD VARS
        QVector<const Variable*> fields;
        varModel->beginInsertRows(QModelIndex(), 0, 15);
        Variable* bw    = new Variable(0, colors-1, tr("Baden-Württemberg"));
        Variable* by    = new Variable(0, colors-1, tr("Bavaria"));
        Variable* be    = new Variable(0, colors-1, tr("Berlin"));
        Variable* bb    = new Variable(0, colors-1, tr("Brandenburg"));
        Variable* hb    = new Variable(0, colors-1, tr("Bremen"));
        Variable* hh    = new Variable(0, colors-1, tr("Hamburg"));
        Variable* he    = new Variable(0, colors-1, tr("Hesse"));
        Variable* mv    = new Variable(0, colors-1, tr("Mecklenburg Western Pomerania"));
        Variable* ni    = new Variable(0, colors-1, tr("Lower Saxony"));
        Variable* nw    = new Variable(0, colors-1, tr("Northrhine-Westphalia"));
        Variable* rp    = new Variable(0, colors-1, tr("Rhineland Palatinate"));
        Variable* sl    = new Variable(0, colors-1, tr("Saarland"));
        Variable* sn    = new Variable(0, colors-1, tr("Saxony"));
        Variable* st    = new Variable(0, colors-1, tr("Saxony-Anhalt"));
        Variable* sh    = new Variable(0, colors-1, tr("Schleswig Holstein"));
        Variable* th    = new Variable(0, colors-1, tr("Thuringia"));
        varModel->items.append(bw);
        varModel->items.append(by);
        varModel->items.append(be);
        varModel->items.append(bb);
        varModel->items.append(hb);
        varModel->items.append(hh);
        varModel->items.append(he);
        varModel->items.append(mv);
        varModel->items.append(ni);
        varModel->items.append(nw);
        varModel->items.append(rp);
        varModel->items.append(sl);
        varModel->items.append(sn);
        varModel->items.append(st);
        varModel->items.append(sh);
        varModel->items.append(th);
        varModel->endInsertRows();

        // ADD CONSTRS
        constrModel->beginInsertRows(QModelIndex(), 0, 1);

        constrModel->items.append(new ConstraintSimpleRelation(bw, RELATIONS::NEQ, rp));
        constrModel->items.append(new ConstraintSimpleRelation(bw, RELATIONS::NEQ, he));
        constrModel->items.append(new ConstraintSimpleRelation(bw, RELATIONS::NEQ, by));

//        constrModel->items.append(new ConstraintSimpleRelation(by, RELATIONS::NEQ, bw));
        constrModel->items.append(new ConstraintSimpleRelation(by, RELATIONS::NEQ, he));
        constrModel->items.append(new ConstraintSimpleRelation(by, RELATIONS::NEQ, th));
        constrModel->items.append(new ConstraintSimpleRelation(by, RELATIONS::NEQ, sn));

        constrModel->items.append(new ConstraintSimpleRelation(be, RELATIONS::NEQ, bb));

//        constrModel->items.append(new ConstraintSimpleRelation(bb, RELATIONS::NEQ, be));
        constrModel->items.append(new ConstraintSimpleRelation(bb, RELATIONS::NEQ, sn));
        constrModel->items.append(new ConstraintSimpleRelation(bb, RELATIONS::NEQ, st));
        constrModel->items.append(new ConstraintSimpleRelation(bb, RELATIONS::NEQ, ni));
        constrModel->items.append(new ConstraintSimpleRelation(bb, RELATIONS::NEQ, mv));

        constrModel->items.append(new ConstraintSimpleRelation(hb, RELATIONS::NEQ, ni));

        constrModel->items.append(new ConstraintSimpleRelation(hh, RELATIONS::NEQ, ni));
        constrModel->items.append(new ConstraintSimpleRelation(hh, RELATIONS::NEQ, sh));

//        constrModel->items.append(new ConstraintSimpleRelation(he, RELATIONS::NEQ, by));
//        constrModel->items.append(new ConstraintSimpleRelation(he, RELATIONS::NEQ, bw));
        constrModel->items.append(new ConstraintSimpleRelation(he, RELATIONS::NEQ, rp));
        constrModel->items.append(new ConstraintSimpleRelation(he, RELATIONS::NEQ, nw));
        constrModel->items.append(new ConstraintSimpleRelation(he, RELATIONS::NEQ, ni));
        constrModel->items.append(new ConstraintSimpleRelation(he, RELATIONS::NEQ, th));

//        constrModel->items.append(new ConstraintSimpleRelation(mv, RELATIONS::NEQ, bb));
        constrModel->items.append(new ConstraintSimpleRelation(mv, RELATIONS::NEQ, ni));
        constrModel->items.append(new ConstraintSimpleRelation(mv, RELATIONS::NEQ, sh));

//        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, hb));
//        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, hh));
        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, nw));
//        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, he));
        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, th));
        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, st));
//        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, bb));
//        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, mv));
        constrModel->items.append(new ConstraintSimpleRelation(ni, RELATIONS::NEQ, sh));

        constrModel->items.append(new ConstraintSimpleRelation(nw, RELATIONS::NEQ, rp));
//        constrModel->items.append(new ConstraintSimpleRelation(nw, RELATIONS::NEQ, he));
//        constrModel->items.append(new ConstraintSimpleRelation(nw, RELATIONS::NEQ, ni));

        constrModel->items.append(new ConstraintSimpleRelation(rp, RELATIONS::NEQ, sl));
//        constrModel->items.append(new ConstraintSimpleRelation(rp, RELATIONS::NEQ, bw));
//        constrModel->items.append(new ConstraintSimpleRelation(rp, RELATIONS::NEQ, he));
//        constrModel->items.append(new ConstraintSimpleRelation(rp, RELATIONS::NEQ, nw));

//        constrModel->items.append(new ConstraintSimpleRelation(sl, RELATIONS::NEQ, rp));

//        constrModel->items.append(new ConstraintSimpleRelation(sn, RELATIONS::NEQ, by));
        constrModel->items.append(new ConstraintSimpleRelation(sn, RELATIONS::NEQ, th));
        constrModel->items.append(new ConstraintSimpleRelation(sn, RELATIONS::NEQ, st));
//        constrModel->items.append(new ConstraintSimpleRelation(sn, RELATIONS::NEQ, bb));

//        constrModel->items.append(new ConstraintSimpleRelation(st, RELATIONS::NEQ, sn));
        constrModel->items.append(new ConstraintSimpleRelation(st, RELATIONS::NEQ, th));
//        constrModel->items.append(new ConstraintSimpleRelation(st, RELATIONS::NEQ, ni));
//        constrModel->items.append(new ConstraintSimpleRelation(st, RELATIONS::NEQ, bb));

//        constrModel->items.append(new ConstraintSimpleRelation(sh, RELATIONS::NEQ, hh));
        constrModel->items.append(new ConstraintSimpleRelation(sh, RELATIONS::NEQ, ni));
//        constrModel->items.append(new ConstraintSimpleRelation(sh, RELATIONS::NEQ, mv));

//        constrModel->items.append(new ConstraintSimpleRelation(th, RELATIONS::NEQ, by));
//        constrModel->items.append(new ConstraintSimpleRelation(th, RELATIONS::NEQ, he));
//        constrModel->items.append(new ConstraintSimpleRelation(th, RELATIONS::NEQ, ni));
//        constrModel->items.append(new ConstraintSimpleRelation(th, RELATIONS::NEQ, sn));
//        constrModel->items.append(new ConstraintSimpleRelation(th, RELATIONS::NEQ, st));

        constrModel->endInsertRows();
    }
}




//////////////////////////////////////////////////////////////////////////////////////////////
///     EXPORT / IMPORT HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////

void CspTab::handleActionExport()
{
    if ( varModel->size() == 0 || constrModel->size() == 0)
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE,
            tr("No problem defined, storing an \"empty\" problem is not allowed"));
        return;
    }

    QFile writeOpenFile;
    if ( !Util::askForFileLocation(this, writeOpenFile, true, true) )
        return;

    QJsonDocument   doc;
    QJsonObject     root;
    QJsonArray      jsonVars;
    QJsonArray      jsonConstrs;

    for ( const Variable* var : *varModel )
    {
        QJsonObject jsonVar;
        var->parseToJson(jsonVar);
        jsonVars.append(jsonVar);
    }

    for ( const AbstractConstraint* constr : *constrModel )
    {
        QJsonObject jsonConstr;
        constr->parseToJson(jsonConstr);
        jsonConstrs.append(jsonConstr);
    }

    root["variables"]   = jsonVars;
    root["constraints"] = jsonConstrs;
    doc.setObject(root);

    writeOpenFile.write(doc.toJson(QJsonDocument::Indented));

    if ( solutions.size() > 0 )
        if (
            QMessageBox::question(this, tr("Export solution"),
            tr("A solution exists, export it too?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes
        )
            Util::saveSolutions(this, ui->solutionSelectionList, ui->solutionList, ui->solutionStatsTxt);

    QMessageBox::information(this, STRINGS::MSG_INFO, tr("Export finished"));
}



void CspTab::handleActionImport()
{
    QFile readOpenFile;
    if ( !Util::askForFileLocation(this, readOpenFile, false, true) )
        return;

    handleVarDeleteAll(); // clear both models & reset id counter

    QJsonDocument   doc         = QJsonDocument::fromJson(readOpenFile.readAll());
    QJsonObject     root        = doc.object();
    QJsonArray      jsonVars    = root.value("variables").toArray();
    QJsonArray      jsonConstrs = root.value("constraints").toArray();

    // vars are parsed first & insert themselves into the map as pointers mapped by their IDs
    // afterwards constraints are parsed & read pointers of vars they contain
    QMap<int, const Variable*> varMap;

    for ( QJsonValue jsonVal : jsonVars )
    {
        QJsonObject jsonVar = jsonVal.toObject();
        Variable*   var     = new Variable(jsonVar, varMap);

        varModel->addItem(var);
    }

    for ( QJsonValue jsonVal : jsonConstrs )
    {
        QJsonObject jsonConstr = jsonVal.toObject();

        AbstractConstraint* constr = nullptr;
        switch ( jsonConstr[JSON::CONSTR_TYPE].toInt() )
        {
            case JSON::ALL_DIFF:    constr = new ConstraintAllDiff(jsonConstr, varMap);         break;
            case JSON::MIN_MAX:     constr = new ConstraintMinMax(jsonConstr, varMap);          break;
            case JSON::MIN_MAX_EQ:  constr = new ConstraintMinMaxEq(jsonConstr, varMap);        break;
            case JSON::MOD_PROD_DIV:constr = new ConstraintModProdDiv(jsonConstr, varMap);      break;
            case JSON::SCALAR:      constr = new ConstraintScalarProd(jsonConstr, varMap);      break;
            case JSON::RELATION:    constr = new ConstraintSimpleRelation(jsonConstr, varMap);  break;
            case JSON::SUM:         constr = new ConstraintSum(jsonConstr, varMap);             break;
        }

        if ( constr != nullptr )
            constrModel->addItem(constr);
    }

    QMessageBox::information(this, STRINGS::MSG_INFO, tr("Import finished"));
}
