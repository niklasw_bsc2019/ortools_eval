/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

#include "util.h"

QString Util::numberListAsString(const QVector<int64>& list, int maxSize,
                                   char separator, char bounds, char bounde)
{
    if ( list.size() == 0 )
        return "[]";

    if ( list.size() > maxSize )
    {
        QString str("[");
        str += QString::number(list[0]);
        str += ", ..., ";
        str += QString::number(list[list.size()-1]) + "]";
        return str;
    }

    QString str(bounds);
    for ( int i = 0; i < list.size(); ++i )
    {
        str += QString::number(list[i]);
        if ( i < list.size()-1 )
            str += separator;
    }
    return str += bounde;
}



void Util::initGroup(QButtonGroup* grp, QRadioButton* rb1, QWidget* w1, QRadioButton* rb2, QWidget* w2)
{
    grp->addButton(rb1);
    grp->addButton(rb2);

    rb1->setChecked(true);
    w1->setEnabled(true);
    rb2->setChecked(false);
    w2->setEnabled(false);

    // tries to call connect(...) in winsock.h for some reason without explicit scoping!?
    QObject::connect(rb1, &QRadioButton::toggled, [w1,w2](bool checked){
        if ( checked )
        {
            w1->setEnabled(true);
            w2->setEnabled(false);
        }
    });

    QObject::connect(rb2, &QRadioButton::toggled, [w1,w2](bool checked){
        if ( checked )
        {
            w1->setEnabled(false);
            w2->setEnabled(true);
        }
    });
}


void Util::initGroup(QButtonGroup* grp, QRadioButton* rb1, QWidget* w1, QRadioButton* rb2, QWidget* w2, QWidget* w3)
{
    grp->addButton(rb1);
    grp->addButton(rb2);

    rb1->setChecked(true);
    w1->setEnabled(true);
    rb2->setChecked(false);
    w2->setEnabled(false);
    w3->setEnabled(false);

    // tries to call connect(...) in winsock.h for some reason without explicit scoping!?
    QObject::connect(rb1, &QRadioButton::toggled, [w1,w2,w3](bool checked){
        if ( checked )
        {
            w1->setEnabled(true);
            w2->setEnabled(false);
            w3->setEnabled(false);
        }
    });

    QObject::connect(rb2, &QRadioButton::toggled, [w1,w2,w3](bool checked){
        if ( checked )
        {
            w1->setEnabled(false);
            w2->setEnabled(true);
            w3->setEnabled(true);
        }
    });
}



Util::addConstrFuncPtr Util::getMethodByCompareSymbol(QString relation)
{
    addConstrFuncPtr func = nullptr;

    if      ( relation == RELATIONS::EQ )           func = &CpModelBuilder::AddEquality;
    else if ( relation == RELATIONS::NEQ )          func = &CpModelBuilder::AddNotEqual;
    else if ( relation == RELATIONS::GREATER )      func = &CpModelBuilder::AddGreaterThan;
    else if ( relation == RELATIONS::GREATER_EQ )   func = &CpModelBuilder::AddGreaterOrEqual;
    else if ( relation == RELATIONS::SMALLER )      func = &CpModelBuilder::AddLessThan;
    else if ( relation == RELATIONS::SMALLER_EQ )   func = &CpModelBuilder::AddLessOrEqual;

    return func;
}



bool Util::askForFileLocation(QWidget* parent, QFile& file, bool save, bool json)
{
    QString fileName;

    if ( save )
    {
        fileName = json
                    ?
                    QFileDialog::getSaveFileName(parent, QObject::tr("Save to JSON file"), "", QObject::tr("JSON Files (*.json);;All Files (*.*)"))
                    :
                    QFileDialog::getSaveFileName(parent, QObject::tr("Save to TXT file"), "", QObject::tr("Text Files (*.txt);;All Files (*.*)"));
    }
    else
    {
        fileName = json
                    ?
                    QFileDialog::getOpenFileName(parent, QObject::tr("Load from JSON file"), "", QObject::tr("JSON Files (*.json);;All Files (*.*)"))
                    :
                    QFileDialog::getOpenFileName(parent, QObject::tr("Load from TXT file"), "", QObject::tr("Text Files (*.txt);;All Files (*.*)"));
    }


    if ( fileName == "" )
       return false;

   file.setFileName(fileName);

   if ( ! file.open(save ? QIODevice::WriteOnly : QIODevice::ReadOnly) )
   {
       QMessageBox::critical(
            parent, STRINGS::ERR_TITLE,
            (save ? QObject::tr("Unable to open file for writing") : QObject::tr("Unable to open file for reading"))
       );
       return false;
   }

   return true;
}



void Util::saveSolutions(QWidget* parent, QListWidget* solutions, QListView* solutionContent, QPlainTextEdit* stats)
{
    QFile writeOpenFile;
    if ( !Util::askForFileLocation(parent, writeOpenFile, true, false) )
        return;

    QTextStream out(&writeOpenFile);

    for ( int i = 0; i < solutions->count(); ++i )
    {
        out << solutions->item(i)->text().toUpper() + "\n\n";

        if ( stats )
        {
            solutions->setCurrentRow(i);
            out << stats->toPlainText() + "\n";
        }

        QAbstractItemModel* model = solutionContent->model();

        for ( int j = 0; j < model->rowCount(); ++j )
            out << model->data(model->index(j, 0, QModelIndex()), Qt::DisplayRole).toString() + "\n";

        out << "\n\n";
    }
}



int64 Util::getMeterDistance(double latA, double lonA, double latB, double lonB)
{
//  Haversine Formula
//  φ is latitude
//  λ is longitude
//  R is earth’s radius
//  a = sin²(φB - φA/2) + cos φA * cos φB * sin²(λB - λA/2)
//  c = 2 * atan2( √a, √(1−a) )
//  d = R ⋅ c

    double rad_latA = qDegreesToRadians(latA);
    double rad_lonA = qDegreesToRadians(lonA);
    double rad_latB = qDegreesToRadians(latB);
    double rad_lonB = qDegreesToRadians(lonB);

    double a = qPow(qSin((rad_latB - rad_latA)/2), 2) + qCos(rad_latA) * qCos(rad_latB) * qPow(qSin((rad_lonB - rad_lonA)/2), 2);
    double c = 2 * qAtan2(qSqrt(a), qSqrt(1-a));

    return static_cast<int64>(EARTH_RADIUS_METER * c);
}
