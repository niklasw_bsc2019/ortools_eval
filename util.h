/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file util.h
 * @author Niklas Wahrenberg
 */

#ifndef UTIL_H
#define UTIL_H

#include <QtCore>
#include <QListView>
#include <QListWidget>
#include <QPlainTextEdit>
#include <QMessageBox>
#include <QFileDialog>
#include <QRadioButton>
#include <QButtonGroup>
#include <QtMath>

#include "ortools/sat/cp_model.h"
using namespace operations_research::sat;



namespace JSON
{
    enum CONSTR_TYPE {
        ALL_DIFF        = 0,
        MIN_MAX,        // 1
        MIN_MAX_EQ,     // 2
        MOD_PROD_DIV,   // 3
        SCALAR,         // 4
        RELATION,       // 5
        SUM             // 6
    };

    static const QString CONSTR_TYPE       = "type";
    static const QString CONSTR_INT_LIST   = "intList";
    static const QString CONSTR_VAR_LIST   = "varList";
    static const QString CONSTR_VAR1       = "var1";
    static const QString CONSTR_VAR2       = "var2";
    static const QString CONSTR_VAR3       = "var3";
    static const QString CONSTR_BOOL       = "bool";
    static const QString CONSTR_RELATION   = "relation";
    static const QString CONSTR_VALUE      = "value";
    static const QString CONSTR_ENUM       = "enumValue";
    static const QString CONSTR_INT        = "int";
}



namespace RELATIONS
{
    static const QString EQ            = "=";
    static const QString NEQ           = "!=";
    static const QString GREATER_EQ    = ">=";
    static const QString GREATER       = ">";
    static const QString SMALLER_EQ    = "<=";
    static const QString SMALLER       = "<";
}



namespace STRINGS
{
    static const QString MSG_INFO               = QObject::tr("Info");

    static const QString MSG_INPUT_REQUIRED     = QObject::tr("Input required");
    static const QString WARN_TITLE             = QObject::tr("Warning");

    static const QString ERR_TITLE              = QObject::tr("Error");
    static const QString ERR_DOMAINLIST_REQUIRED= QObject::tr("Domain list must be specified");
    static const QString ERR_NO_ITEM_SELECTED   = QObject::tr("No item selected");
    static const QString ERR_VEC_SIZE_NOT_EQ    = QObject::tr("Input vectors must be of equal size");
    static const QString ERR_VAR_LIMIT          = QObject::tr("Limit for variables reached");
    static const QString ERR_MISSING_INPUT      = QObject::tr("Missing input");
    static const QString ERR_NOT_ENOUGH_SELECTED= QObject::tr("More than one items must be selected");
    static const QString ERR_VARS_MUST_EXIST    = QObject::tr("At least variables must be defined before solving");
}


class Util
{
    typedef __int64 int64;    

private:
    Util();

public:

    // constraint type scoped to avoid: "ambiguous symbol operations_research::Constraint or operations_research::sat::Constraint"
    typedef operations_research::sat::Constraint (CpModelBuilder::*addConstrFuncPtr)(const LinearExpr& left, const LinearExpr& right);

    // []           ->  "[]"
    // [1,...,51]   ->  "[1,...,51]"
    // [1,2,3,4]    ->  "[1,2,3,4]"
    static QString numberListAsString(
            const QVector<int64>& list, int maxSize = 20,
            char separator = ',', char bounds = '[', char bounde = ']'
        );



    // add rb1 & rb2 to grp and create connection dis/enabling w1 & w2 depending on which rb is checked
    static void initGroup(QButtonGroup* grp, QRadioButton* rb1, QWidget* w1,
                                             QRadioButton* rb2, QWidget* w2);

    static void initGroup(QButtonGroup* grp, QRadioButton* rb1, QWidget* w1,
                                             QRadioButton* rb2, QWidget* w2, QWidget* w3);


    static addConstrFuncPtr getMethodByCompareSymbol(QString relation);


    static bool askForFileLocation(QWidget* parent, QFile& file, bool save, bool json);


    static void saveSolutions(QWidget* parent, QListWidget* solutions, QListView* solutionContent, QPlainTextEdit* stats = nullptr);


    template<class T>
    static void getMinMaxAvgMedian(QVector<T>& src, int& min, int& max, int& avg, int& med)
    {
        min = *std::min_element(begin(src), end(src));
        max = *std::max_element(begin(src), end(src));
        avg = std::accumulate(begin(src), end(src), T()) / src.count();
        std::sort(begin(src), end(src));
        med = src.count() % 2 == 1 ? src[src.count()/2] : (src[src.count()/2-1] + src[src.count()/2]) / 2;
    }


    // a <-> b
    template<class T>
    static void swapValues(T& a, T& b)
    {
        T tmp = a;
        a = b;
        b = tmp;
    }



    static constexpr double EARTH_RADIUS_METER = 6371e3;
    static           int64  getMeterDistance(double latA, double lonA, double latB, double lonB); // using Haversine Formula
};

#endif // UTIL_H
