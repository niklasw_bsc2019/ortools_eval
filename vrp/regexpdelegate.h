/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file regexpdelegate.h
 * @author Niklas Wahrenberg
 */

#ifndef LOCATIONDELEGATE_H
#define LOCATIONDELEGATE_H

#include <QtCore>
#include <QtWidgets>
#include <QtGui>
#include <QStyledItemDelegate>

// https://doc.qt.io/qt-5/model-view-programming.html
// QAbstractItemDelegate is the abstract base class for delegates in the model/view framework.
// The default delegate implementation is provided by QStyledItemDelegate, and this is used as the default delegate by Qt's standard views.
// However, QStyledItemDelegate and QItemDelegate are independent alternatives to painting and providing editors for items in views.
// The difference between them is that QStyledItemDelegate uses the current style to paint its items.
// We therefore recommend using QStyledItemDelegate as the base class when implementing custom delegates or when working with Qt style sheets.
class RegExpDelegate : public QStyledItemDelegate
{

private:
    QRegExpValidator* regExpValidator;

public:

    RegExpDelegate(QObject *parent, QRegExpValidator* regExpValidator) : QStyledItemDelegate(parent), regExpValidator(regExpValidator){}

//    https://doc.qt.io/qt-5/qstyleditemdelegate.html
//    It is possible for a custom delegate to provide editors without the use of an editor item factory.
//    In this case, the following virtual functions must be reimplemented
    QWidget*    createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index)         const override;
    void        setEditorData(QWidget *editor, const QModelIndex &index)                                            const override;
    void        setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index)                  const override;
    void        updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};


QWidget* RegExpDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QLineEdit* lineEdit = new QLineEdit(index.model()->data(index, Qt::DisplayRole).toString(), parent);
    lineEdit->setValidator(regExpValidator);
    return lineEdit;
}

void RegExpDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    static_cast<QLineEdit*>(editor)->setText(index.model()->data(index, Qt::DisplayRole).toString());
}

void RegExpDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    model->setData(index, static_cast<QLineEdit*>(editor)->text(), Qt::DisplayRole);
}

void RegExpDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
}

#endif // LOCATIONDELEGATE_H
