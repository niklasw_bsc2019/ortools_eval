/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

#include "vrptab.h"
#include "regexpdelegate.h"

#include "ortools/constraint_solver/routing_enums.pb.h"
using namespace operations_research;


VrpTab::VrpTab(QWidget *parent)
    :
    QTabWidget(parent),
    ui(new Ui::VrpTab),    
    singleMultiDepotGroup(new QButtonGroup(this)),
    locationDistMatGroup(new QButtonGroup(this)),
    vrpSolverThread(nullptr),
    locationModel(new QStringListModel(this)),    
    solutionModel(new QStringListModel(this))
{
    ui->setupUi(this);

    locationDistMatGroup->addButton(ui->locationBox);
    locationDistMatGroup->addButton(ui->problemAsTextBox);

    singleMultiDepotGroup->addButton(ui->singleDepotBox);
    singleMultiDepotGroup->addButton(ui->multiDepotBox);

    initModels();
    initConnections();
    initValidators();
}



VrpTab::~VrpTab()
{
    ui->webEngineView->stop();
    delete ui;
}



bool VrpTab::event(QEvent *event)
{
    if ( event->type() == QEvent::KeyPress && ui->locationTxt->hasFocus() )
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if ( keyEvent->key() == Qt::Key_Enter || keyEvent->key() == Qt::Key_Return )
            handleAddLocation();
    }

    return QWidget::event(event);
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     UI SETUP
//////////////////////////////////////////////////////////////////////////////////////////////

static const QMap<QString, FirstSolutionStrategy_Value> firstSolutionStrategies = {
    { "AUTOMATIC",                      FirstSolutionStrategy_Value_AUTOMATIC                       },
    { "PATH_CHEAPEST_ARC",              FirstSolutionStrategy_Value_PATH_CHEAPEST_ARC               },
    { "PATH_MOST_CONSTRAINED_ARC",      FirstSolutionStrategy_Value_PATH_MOST_CONSTRAINED_ARC       },
    { "EVALUATOR_STRATEGY",             FirstSolutionStrategy_Value_EVALUATOR_STRATEGY              },
    { "SAVINGS",                        FirstSolutionStrategy_Value_SAVINGS                         },

    { "SWEEP",                          FirstSolutionStrategy_Value_SWEEP                           },
    { "CHRISTOFIDES",                   FirstSolutionStrategy_Value_CHRISTOFIDES                    },
    { "ALL_UNPERFORMED",                FirstSolutionStrategy_Value_ALL_UNPERFORMED                 },
    { "BEST_INSERTION",                 FirstSolutionStrategy_Value_BEST_INSERTION                  },
    { "PARALLEL_CHEAPEST_INSERTION",    FirstSolutionStrategy_Value_PARALLEL_CHEAPEST_INSERTION     },

    { "SEQUENTIAL_CHEAPEST_INSERTION",  FirstSolutionStrategy_Value_SEQUENTIAL_CHEAPEST_INSERTION   },
    { "LOCAL_CHEAPEST_INSERTION",       FirstSolutionStrategy_Value_LOCAL_CHEAPEST_INSERTION        },
    { "GLOBAL_CHEAPEST_ARC",            FirstSolutionStrategy_Value_GLOBAL_CHEAPEST_ARC             },
    { "LOCAL_CHEAPEST_ARC",             FirstSolutionStrategy_Value_LOCAL_CHEAPEST_ARC              },
    { "FIRST_UNBOUND_MIN_VALUE",        FirstSolutionStrategy_Value_FIRST_UNBOUND_MIN_VALUE         }
};

static const QMap<QString, LocalSearchMetaheuristic_Value> localSearchStrategies = {
    { "AUTOMATIC",              LocalSearchMetaheuristic_Value_AUTOMATIC            },
    { "GREEDY_DESCENT",         LocalSearchMetaheuristic_Value_GREEDY_DESCENT       },
    { "GUIDED_LOCAL_SEARCH",    LocalSearchMetaheuristic_Value_GUIDED_LOCAL_SEARCH  },
    { "SIMULATED_ANNEALING",    LocalSearchMetaheuristic_Value_SIMULATED_ANNEALING  },
    { "TABU_SEARCH",            LocalSearchMetaheuristic_Value_TABU_SEARCH          },
    { "OBJECTIVE_TABU_SEARCH",  LocalSearchMetaheuristic_Value_GENERIC_TABU_SEARCH  }
};

void VrpTab::initModels()
{
    ui->locationList->setModel(locationModel);
    ui->locationList->setEditTriggers(QAbstractItemView::DoubleClicked);
    ui->singleDepotLocationBox->setModel(locationModel);

    ui->strategyFirstSolutionBox->addItems(firstSolutionStrategies.keys());
    ui->strategyFirstSolutionBox->setCurrentText("AUTOMATIC");
    ui->strategyLocalSerachBox->addItems(localSearchStrategies.keys());
    ui->strategyLocalSerachBox->setCurrentText("AUTOMATIC");

    ui->solutionRouteList->setModel(solutionModel);
}



void setSingleDepotEnabled(Ui::VrpTab* ui, bool enabled)
{
    ui->singleDepotLbl->setEnabled(enabled);
    ui->singleDepotLbl1->setEnabled(enabled);
    ui->singleDepotLbl2->setEnabled(enabled);
    ui->singleDepotLbl3->setEnabled(enabled);
    ui->singleDepotLocationBox->setEnabled(enabled);
    ui->singleDepotVehicleCountSp->setEnabled(enabled);
    ui->singleDepotVehicleCapacitySp->setEnabled(enabled);
}

void setMultiDepotEnabled(Ui::VrpTab* ui, bool enabled)
{
    ui->multiDepotLbl->setEnabled(enabled);
    ui->multiDepotLbl1->setEnabled(enabled);
    ui->multiDepotLbl2->setEnabled(enabled);
    ui->multiDepotLbl3->setEnabled(enabled);
    ui->multiDepotLocationTxt->setEnabled(enabled);
    ui->multiDepotVehicleCountTxt->setEnabled(enabled);
    ui->multiDepotVehicleCapacitySp->setEnabled(enabled);
}

void VrpTab::initConnections()
{
    // LOCATIONS TAB

    connect(ui->locationBox, &QCheckBox::toggled, [this](bool latLonLocationEnabled){

       // latLonLocationEnabled

       ui->locationHeaderLbl->setEnabled(latLonLocationEnabled);       
       ui->locationTxt->setEnabled(latLonLocationEnabled);
       ui->locationAddBtn->setEnabled(latLonLocationEnabled);
       ui->locationList->setEnabled(latLonLocationEnabled);
       ui->locationDelBtn->setEnabled(latLonLocationEnabled);
       ui->locationDelAllBtn->setEnabled(latLonLocationEnabled);

       ui->singleDepotBox->setEnabled(latLonLocationEnabled);
       ui->multiDepotBox->setEnabled(latLonLocationEnabled);

       if       ( ui->singleDepotBox->isChecked() ) setSingleDepotEnabled(ui, latLonLocationEnabled);
       else if  ( ui->multiDepotBox->isChecked()  ) setMultiDepotEnabled(ui, latLonLocationEnabled);



       // !latLonLocationEnabled

       ui->problemAsTextHeaderLbl->setEnabled(!latLonLocationEnabled);
       ui->problemAsTextDefineBtn->setEnabled(!latLonLocationEnabled);
    });
    ui->locationBox->setChecked(true);


    connect(ui->singleDepotBox, &QCheckBox::toggled, [this](bool singleDepotEnabled){        
        setSingleDepotEnabled(ui, singleDepotEnabled);
        setMultiDepotEnabled(ui, !singleDepotEnabled);
    });
    ui->singleDepotBox->setChecked(true);


    connect(ui->locationAddBtn,     &QPushButton::clicked, this, &VrpTab::handleAddLocation);
    connect(ui->locationDelBtn,     &QPushButton::clicked, this, &VrpTab::handleDeleteLocation);
    connect(ui->locationDelAllBtn,  &QPushButton::clicked, this, &VrpTab::handleDeleteAllLocations);


    connect(ui->problemAsTextDefineBtn, &QPushButton::clicked, [this](){
        bool ok;
        QString newDistMatTxt = QInputDialog::getMultiLineText(this,
                                "Distance matrix","Enter the distance matrix (& optionally the demands in the last line): ",
                                problemTxt, &ok);

        if ( ok )
            problemTxt = newDistMatTxt;
    });



    // SOLVE TAB

    connect(ui->solveBtn,           &QPushButton::clicked, this, &VrpTab::handleSolve);
    connect(ui->solutionClearBtn,   &QPushButton::clicked, this, &VrpTab::handleSolutionClear);

    connect(ui->solutionSelectionList, &QListWidget::currentRowChanged, [this](int currentRow){
        if ( currentRow < solutions.size() )
            solutionModel->setStringList(solutions[currentRow]);
    });
}



void VrpTab::initValidators()
{
    QRegExpValidator* regExpValidator = new QRegExpValidator(
                                                QRegExp(
                                                    "([a-zA-Z0-9]+\\d?(,|;) *)"                                 // location name [optionally numbered] followed by , or ; and 0 to n spaces/tabs
                                                    "-?(\\d|[1-8][0-9]|90)(\\.\\d+)?"                           // latitude: [-] 0 to 90 [._______]
                                                    "((,|;) *)"                                                 // , or ; and 0 to n spaces/tabs
                                                    "-?(\\d{1,2}|1[0-7][0-9]|180)(\\.\\d+)?"                    // longitude: [-] 0 to 180 [._______]
                                                    "(((,|;) *)\\d+)?"                                          // [, or ; and 0 to n spaces/tabs followed by location demand]
                                                ),
                                                this
                                            );
    ui->locationTxt->setValidator(regExpValidator);
    ui->locationList->setItemDelegate(new RegExpDelegate(this, regExpValidator));

    ui->multiDepotLocationTxt->setValidator(new QRegExpValidator(QRegExp("(\\d+)((,|;) *(\\d+))*"), this));
    ui->multiDepotVehicleCountTxt->setValidator(new QRegExpValidator(QRegExp("(\\d+)((,|;) *(\\d+))*"), this));
}



void VrpTab::setListSelectionMode(QAbstractItemView::SelectionMode mode)
{
    ui->locationList->setSelectionMode(mode);
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     LOCATION EDITING
//////////////////////////////////////////////////////////////////////////////////////////////

void VrpTab::handleAddLocation()
{
    if (
        ui->locationTxt->text().split(QRegExp(",|;")).size() != 4
        &&
        (
            (ui->singleDepotBox->isChecked() && ui->singleDepotVehicleCapacitySp->value() > 0) ||
            (ui->multiDepotBox->isChecked() && ui->multiDepotVehicleCapacitySp->value() > 0)
        )
    )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("Expected input format is <LocationName>,<latDeg>,<lonDeg>,<maxDemandPerLocation>"));
        return;
    }
    else if ( ui->locationTxt->text().split(QRegExp(",|;")).size() != 3 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("Expected input format is <LocationName>,<latDeg>,<lonDeg>"));
        return;
    }

    QStringList modelBackend = locationModel->stringList();
    modelBackend << ui->locationTxt->text();
    locationModel->setStringList(modelBackend);
}



void VrpTab::handleDeleteLocation()
{
    QStringList     modelBackend = locationModel->stringList();
    QModelIndexList indexList    = ui->locationList->selectionModel()->selectedIndexes();
    for ( int i = indexList.size()-1; i >= 0; --i )
        modelBackend.removeAt(indexList[i].row());
    locationModel->setStringList(modelBackend);
}



void VrpTab::handleDeleteAllLocations()
{
    QStringList modelBackend = locationModel->stringList();
    modelBackend.clear();
    locationModel->setStringList(modelBackend);
    ui->locationTxt->clear();
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     SOLVE HANDLER HELPER
//////////////////////////////////////////////////////////////////////////////////////////////


// helper method for VrpTab::parseProblemUiFormAndBuildSolverThread()
bool VrpTab::parseListView(QVector<QString>& names, QVector<QVector<int64>>& distMat, QVector<int64>& demands)
{
    QVector<QString> locationStrings = locationModel->stringList().toVector();

    if ( locationStrings.count() < 3 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("At least 3 locations must be defined"));
        return false;
    }

    // put infos from each "LocationName, Lat, Lon, Demand"-string into vectors
    QVector<QPair<double,double>>   latLons;
    bool                            capacitySpecified = ( ui->singleDepotBox->isChecked() && ui->singleDepotVehicleCapacitySp->value() > 0 )
                                                        ||
                                                        ( ui->multiDepotBox->isChecked() && ui->multiDepotVehicleCapacitySp->value() > 0 );

    int stringsCount = locationStrings.count();
    names.reserve(stringsCount);
    latLons.reserve(stringsCount);
    if ( capacitySpecified )
        demands.reserve(stringsCount);

    for ( int i = 0; i < locationStrings.count(); ++i )
    {
        QVector<QString> locationAttribs = locationStrings[i].trimmed().split(QRegExp(",|;"), QString::SkipEmptyParts).toVector();

        names   << locationAttribs[0].trimmed();
        latLons << QPair<double,double>(
                        locationAttribs[1].trimmed().toDouble(),
                        locationAttribs[2].trimmed().toDouble()
                    );

        if ( capacitySpecified )
        {
            if ( locationAttribs.count() < 4 )
            {
                QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("When a vehicle capacity greater zero is given each location must have a demand specified (which is 0 for depots), "
                                                                "demand is missing for location \"%0\"").arg(names[i]));
                return false;
            }
            else
                demands << locationAttribs[3].trimmed().toLong();
        }
    }




    // build distance matrix
    QVector<int64> tmp;
    int latLonCount = latLons.count();
    distMat.reserve(latLonCount);
    for ( int i = 0; i < latLonCount; ++i )
    {
        tmp.clear();
        tmp.reserve(latLonCount);
        for ( int j = 0; j < latLonCount; ++j )
                tmp << ( i == j ? 0 : Util::getMeterDistance(latLons[i].first, latLons[i].second, latLons[j].first, latLons[j].second) );
        distMat << tmp;
    }

    return true;
}



bool VrpTab::parseProblemUiFormAndBuildSolverThread()
{
    if ( ui->singleDepotBox->isChecked() && ui->singleDepotLocationBox->currentIndex() == -1 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("A depot must be specified as starting point for the route"));
        return false;
    }




    QVector<QString>        names;
    QVector<QVector<int64>> distMat;
    QVector<int64>          demands;
    if ( !parseListView(names, distMat, demands) )
        return false;




    // create solver thread obj

    if ( ui->singleDepotBox->isChecked() )
    {
        demands[ui->singleDepotLocationBox->currentIndex()] = 0;

        vrpSolverThread = new VrpSolverThread(
                            ui->limitSolutionSp->value(),
                            ui->limitTotalSearchSp->value(),
                            ui->limitLocalSerachSP->value(),

                            firstSolutionStrategies[ui->strategyFirstSolutionBox->currentText()],
                            localSearchStrategies[ui->strategyLocalSerachBox->currentText()],
                            ui->detailedRoutes->isChecked(),

                            names,                                                                      // location names
                            distMat,                                                                    // distances between locations
                            demands,                                                                    // demands at locations in distMat

                            ui->singleDepotVehicleCapacitySp->value(),                                  // capacity of each vehicle in fleet
                            ui->singleDepotVehicleCountSp->value(),                                     // fleet size
                            RoutingIndexManager::NodeIndex{ui->singleDepotLocationBox->currentIndex()}  // depot index
                        );
    }
    else
    {
//         the following code builds the vehicleStartEnd vectors, similar to those below,
//         however only 1 vector is built and used for both, start & end,
//         meaning that start & end locations for a vehicle are always the same
//         (each entry is one vehicle)
//         https://developers.google.com/optimization/routing/routing_tasks

//        const std::vector<RoutingIndexManager::NodeIndex> starts{
//            RoutingIndexManager::NodeIndex{1},
//            RoutingIndexManager::NodeIndex{2},
//            RoutingIndexManager::NodeIndex{15},
//            RoutingIndexManager::NodeIndex{16},
//        };
//        const std::vector<RoutingIndexManager::NodeIndex> ends{
//            RoutingIndexManager::NodeIndex{0},
//            RoutingIndexManager::NodeIndex{0},
//            RoutingIndexManager::NodeIndex{0},
//            RoutingIndexManager::NodeIndex{0},
//        };


        QVector<int> depotIndexes;
        for ( QString depotIndex : ui->multiDepotLocationTxt->text().split(QRegExp(",|;"), QString::SkipEmptyParts) )
                depotIndexes << depotIndex.trimmed().toInt();

        for ( int depotIndex : depotIndexes )
            demands[depotIndex] = 0;

        QVector<int> vehicleCountsPerDepot;
        for ( QString vehicleCountPerDepot : ui->multiDepotVehicleCountTxt->text().split(QRegExp(",|;"), QString::SkipEmptyParts) )
                vehicleCountsPerDepot << vehicleCountPerDepot.trimmed().toInt();

        if ( depotIndexes.count() != vehicleCountsPerDepot.count() )
        {
            QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("For each depot a vehicle count must be specified"));
            return false;
        }

        QVector<RoutingIndexManager::NodeIndex> vehicleDepots;
        for ( int i = 0; i < depotIndexes.count(); ++i )
        {
            int j = 0;
            while ( j++ < vehicleCountsPerDepot[i] )
                vehicleDepots << RoutingIndexManager::NodeIndex{ depotIndexes[i] };
        }




        vrpSolverThread = new VrpSolverThread(
                            ui->limitSolutionSp->value(),
                            ui->limitTotalSearchSp->value(),
                            ui->limitLocalSerachSP->value(),

                            firstSolutionStrategies[ui->strategyFirstSolutionBox->currentText()],
                            localSearchStrategies[ui->strategyLocalSerachBox->currentText()],
                            ui->detailedRoutes->isChecked(),

                            names,                                              // location names
                            distMat,                                            // distances between locations
                            demands,                                            // demands at locations in distMat

                            ui->multiDepotVehicleCapacitySp->value(),           // capacity of each vehicle in fleet
                            vehicleDepots                                       // list of depot indexes
                        );
    }

    return true;
}




static QRegExp depotMatcher("^\\d+\\([1-9]\\d*\\)((,|;) *\\d+\\([1-9]\\d*\\))*$");
static QRegExp numberMatcher("^(\\d+(,|;) *){2,}\\d+$");
static QRegExp capacityMatcher("^[1-9]\\d*$");
bool VrpTab::parseProblemTextAndBuildSolverThread()
{
    if ( problemTxt.length() == 0 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("Problem text is empty"));
        return false;
    }




    QVector<QString>            names;
    QVector<QPair<int ,int>>    depotIndexesAndVehicleCounts;
    QVector<QVector<int64>>     distMat;
    QVector<int64>              demands;
    int                         capacity = 0;
    int                         totalExpectedDistMatLines = 0;


    for ( QString line : problemTxt.split("\n") )
    {
        if ( line.startsWith("//") || line.isEmpty() )  // ignore comment line
            continue;

        if ( line.contains("//") )                      // strip off trailing comments
            line = line.left(line.indexOf("//"));

        line.replace(QRegExp(" +"), "");                // remove all spaces


        // parse depot line
        if ( depotIndexesAndVehicleCounts.count() == 0 )
        {
            if ( !depotMatcher.exactMatch(line))
            {
                QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("Depot line \"%1\"\ndid not pass: %2").arg(line).arg(depotMatcher.pattern()));
                return false;
            }

            for ( QString depotString : line.split(QRegExp("(,|;)"), QString::SkipEmptyParts) )
            {
                QStringList depotAndVehicle = depotString.split("(");
                depotIndexesAndVehicleCounts << QPair<int, int>(depotAndVehicle[0].toInt(), depotAndVehicle[1].replace(")","").toInt());
            }
        }


        // parse distance matrix lines
        else if ( distMat.count() == 0 || distMat.count() != totalExpectedDistMatLines )
        {
            if ( !numberMatcher.exactMatch(line))
            {
                QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("Distance matrix line \"%1\"\ndid not pass: %2").arg(line).arg(numberMatcher.pattern()));
                return false;
            }

            QVector<int64> tmp;
            for ( QString distance : line.split(QRegExp("(,|;)"), QString::SkipEmptyParts) )
                tmp << distance.toLong();

            if ( totalExpectedDistMatLines != 0 && tmp.count() != totalExpectedDistMatLines )   // check for squareness of matrix
            {
                QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("First row of distance matrix had %1 distances, all other lines must have the same count of distances").arg(totalExpectedDistMatLines));
                return false;
            }

            if ( totalExpectedDistMatLines == 0 )
                totalExpectedDistMatLines = tmp.count();

            distMat << tmp;
        }


        // parse demand line
        else if ( demands.count() == 0 )
        {
            if ( !numberMatcher.exactMatch(line))
            {
                QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("Demands line \"%1\"\ndid not pass: %2").arg(line).arg(numberMatcher.pattern()));
                return false;
            }

            for ( QString distance : line.split(QRegExp("(,|;)"), QString::SkipEmptyParts) )
                demands << distance.toLong();

            if ( distMat.count() != demands.count() )
            {
                QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("When demands are given, there must be a demand specified for each location"));
                return false;
            }
        }


        // parse capacity line
        else if ( capacity == 0 )
        {
            if ( !capacityMatcher.exactMatch(line))
            {
                QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("Demands line \"%1\"\ndid not pass: %2").arg(line).arg(numberMatcher.pattern()));
                return false;
            }

            capacity = line.toInt();
        }
    }




    if ( depotIndexesAndVehicleCounts.count() == 0 || distMat.count() == 0 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("At least depot(s) and distance matrix must be specified"));
        return false;
    }
    if ( demands.count() > 0 && capacity == 0 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("When demands are specified also the capacity must be specified"));
        return false;
    }




    for ( int distMatRow = 0; distMatRow < distMat.count(); ++distMatRow )
     names << "Location" + QString::number(distMatRow);




//    // create solver thread obj

    if ( depotIndexesAndVehicleCounts.count() == 1 )
    {
        vrpSolverThread = new VrpSolverThread(
                            ui->limitSolutionSp->value(),
                            ui->limitTotalSearchSp->value(),
                            ui->limitLocalSerachSP->value(),

                            firstSolutionStrategies[ui->strategyFirstSolutionBox->currentText()],
                            localSearchStrategies[ui->strategyLocalSerachBox->currentText()],
                            ui->detailedRoutes->isChecked(),

                            names,                                                                  // location names
                            distMat,                                                                // distances between locations
                            demands,                                                                // demands at locations in distMat

                            capacity,                                                               // capacity of each vehicle in fleet
                            depotIndexesAndVehicleCounts[0].second,                                 // fleet size
                            RoutingIndexManager::NodeIndex{depotIndexesAndVehicleCounts[0].first}   // depot index
                        );
    }
    else
    {
        QVector<RoutingIndexManager::NodeIndex> vehicleDepots;

        for ( QPair<int, int> depotIndexAndVehicleCount : depotIndexesAndVehicleCounts )
        {
            for ( int vehicleNr = 0; vehicleNr < depotIndexAndVehicleCount.second; ++vehicleNr )
                vehicleDepots << RoutingIndexManager::NodeIndex{depotIndexAndVehicleCount.first};
        }

        vrpSolverThread = new VrpSolverThread(
                            ui->limitSolutionSp->value(),
                            ui->limitTotalSearchSp->value(),
                            ui->limitLocalSerachSP->value(),

                            firstSolutionStrategies[ui->strategyFirstSolutionBox->currentText()],
                            localSearchStrategies[ui->strategyLocalSerachBox->currentText()],
                            ui->detailedRoutes->isChecked(),

                            names,                                                                  // location names
                            distMat,                                                                // distances between locations
                            demands,                                                                // demands at locations in distMat

                            capacity,                                                               // capacity of each vehicle in fleet
                            vehicleDepots                                                           // list of depot indexes
                        );
    }

    return true;
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     SOLVE HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////

void VrpTab::handleSolve()
{

    if ( ui->locationBox->isChecked() )
    {
        if ( !parseProblemUiFormAndBuildSolverThread() )
            return;
    }
    else if ( ui->problemAsTextBox->isChecked() )
    {
        if ( !parseProblemTextAndBuildSolverThread() )
            return;
    }

    if ( vrpSolverThread )
        startSolverThread();
}



void VrpTab::startSolverThread()
{
    ui->solveBtn->setEnabled(false);
    ui->solutionClearBtn->setEnabled(false);

    connect(vrpSolverThread, &VrpSolverThread::resultReady, this, &VrpTab::handleResultReceived);
    connect(vrpSolverThread, &QThread::finished, [this](){
        ui->solveBtn->setEnabled(true);
        ui->solutionClearBtn->setEnabled(true);
        vrpSolverThread->deleteLater();
        vrpSolverThread = nullptr;
    });

    handleSolutionClear();
    vrpSolverThread->start();
}



void VrpTab::handleResultReceived(int solutionNr, QStringList route)
{
    setTabText(1, tr("Solutions (") + QString::number(solutionNr) + ")");
    ui->solutionSelectionList->addItem(QString(tr("Solution Nr.") + QString::number(solutionNr)));
    solutions << route;
}



void VrpTab::handleSolutionClear()
{
    setTabText(1, tr("Solutions"));

    solutionModel->removeRows(0, solutionModel->rowCount());
    solutions.clear();

    ui->solutionSelectionList->selectionModel()->reset();
    ui->solutionSelectionList->clear();
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     LOCATION GENERATION
//////////////////////////////////////////////////////////////////////////////////////////////
#include <random>
// https://gist.github.com/graydon/11198540
// line 45: 'DE': ('Germany', (5.98865807458, 47.3024876979, 15.0169958839, 54.983104153)),
void generateLocationsAndDemandsForVrp(VrpTab *vrpTab, QStringListModel *locationModel, int maxDemand /*pass 0 for demandless problem*/)
{
    bool ok;
    int locationCount = QInputDialog::getInt(vrpTab, STRINGS::MSG_INPUT_REQUIRED, QObject::tr("Number of locations:"), 25, 3, 500, 1, &ok);
    if ( !ok )
        return;

    vrpTab->handleDeleteAllLocations();

    int totalDemand = 0;
    int locationNr  = 0;

    std::default_random_engine randomEngine;
    std::uniform_real_distribution<double> latRng(47.3024876979, 54.983104153);
    std::uniform_real_distribution<double> lonRng(5.98865807458, 15.0169958839);

    while ( locationNr < locationCount )
    {
        QStringList modelBackend = locationModel->stringList();

//        int lat = 47 + rand() % 8;
//        int lon = 6 + rand() % 9;

        double lat = latRng(randomEngine);
        double lon = lonRng(randomEngine);

        QString locationString("L" + QString::number(locationNr) + ", " + QString::number(lat) + ", " + QString::number(lon));

        QString demand("");
        if ( maxDemand > 0 )
        {
            if ( maxDemand == 1 )
            {
                demand += "1";
                totalDemand += 1;
            }
            else
            {
                int tmp = 1 + rand() % maxDemand;
                demand = QString::number(tmp);
                totalDemand += tmp;
            }

            locationString += ", " + demand;
        }

        modelBackend << locationString;
        locationModel->setStringList(modelBackend);

        ++locationNr;
    }

    vrpTab->getUi()->singleDepotLocationBox->setCurrentIndex(0);
    vrpTab->getUi()->multiDepotLocationTxt->setText("0");

    QMessageBox::information(vrpTab, STRINGS::MSG_INFO, QObject::tr("Generated a total demand of %0").arg(QString::number(totalDemand)));
}


void VrpTab::generateLocations()
{
    generateLocationsAndDemandsForVrp(this, locationModel, 0);
}



void VrpTab::generateLocationsAndDemands()
{
    bool ok;
    int maxDemand = QInputDialog::getInt(this, STRINGS::MSG_INPUT_REQUIRED, tr("Max demand per location:"), 10, 1, 50, 1, &ok);
    if ( !ok )
        return;

    generateLocationsAndDemandsForVrp(this, locationModel, maxDemand);
}





//////////////////////////////////////////////////////////////////////////////////////////////
///     EXPORT / IMPORT HANDLERS
//////////////////////////////////////////////////////////////////////////////////////////////

void VrpTab::handleActionExport()
{
    if ( locationModel->rowCount() == 0 )
    {
        QMessageBox::critical(this, STRINGS::ERR_TITLE, tr("No problem defined, storing an \"empty\" problem is not allowed"));
        return;
    }

    QFile writeOpenFile;
    if ( !Util::askForFileLocation(this, writeOpenFile, true, true) )
        return;

    QJsonDocument   doc;
    QJsonObject     root;
    QJsonArray      locations;

    for ( QString& location : locationModel->stringList() )
        locations.append(location);

    root["locations"]   = locations;
    root["singleDepot"] = ui->singleDepotBox->isChecked();

    if ( ui->singleDepotBox->isChecked() )
    {
        root["depot"]           = ui->singleDepotLocationBox->currentIndex();
        root["vehicleCount"]    = ui->singleDepotVehicleCountSp->value();
        root["vehicleCapacity"] = ui->singleDepotVehicleCapacitySp->value();
    }
    else
    {
        root["depots"]          = ui->multiDepotLocationTxt->text();
        root["vehicleCounts"]   = ui->multiDepotVehicleCountTxt->text();
        root["vehicleCapacity"] = ui->singleDepotVehicleCapacitySp->value();
    }

    doc.setObject(root);

    writeOpenFile.write(doc.toJson(QJsonDocument::Indented));

    if ( solutions.size() > 0 )
        if ( QMessageBox::question(this, tr("Export solution"), tr("A solution exists, export it too?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes )
            Util::saveSolutions(this, ui->solutionSelectionList, ui->solutionRouteList);

    QMessageBox::information(this, STRINGS::MSG_INFO, tr("Export finished"));
}



void VrpTab::handleActionImport()
{
    QFile readOpenFile;
    if ( !Util::askForFileLocation(this, readOpenFile, false, true) )
        return;

    handleDeleteAllLocations();

    QJsonDocument   doc         = QJsonDocument::fromJson(readOpenFile.readAll());
    QJsonObject     root        = doc.object();
    QJsonArray      locations   = root.value("locations").toArray();

    QStringList modelBackend;
    for ( QJsonValue location : locations )
        modelBackend << location.toString();
    locationModel->setStringList(modelBackend);

    bool singleDepot = root.value("singleDepot").toBool();

    if ( singleDepot )
    {
        ui->singleDepotBox->setChecked(true);
        ui->singleDepotLocationBox->setCurrentIndex(root.value("depot").toInt());
        ui->singleDepotVehicleCountSp->setValue(root.value("vehicleCount").toInt());
        ui->singleDepotVehicleCapacitySp->setValue(root.value("vehicleCapacity").toInt());
    }
    else
    {
        ui->multiDepotBox->setChecked(true);
        ui->multiDepotLocationTxt->setText(root.value("depots").toString());
        ui->multiDepotVehicleCountTxt->setText(root.value("vehicleCounts").toString());
        ui->multiDepotVehicleCapacitySp->setValue(root.value("vehicleCapacity").toInt());
    }

    QMessageBox::information(this, STRINGS::MSG_INFO, tr("Import finished"));
}

