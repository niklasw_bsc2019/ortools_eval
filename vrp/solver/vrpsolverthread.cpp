/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

#include "vrp/solver/vrpsolverthread.h"
#include "util.h"



VrpSolverThread::VrpSolverThread(
    int solutionLimit, int64 localSearchLimitSec, int64 totalSearchLimitSec,
    FirstSolutionStrategy_Value firstSolutionStrategy, LocalSearchMetaheuristic_Value localSearchStrategy, bool detailedRoutes,
    QVector<QString> names, QVector<QVector<int64>> distMat, QVector<int64> demands, int capacity
)
:
    solutionLimit(solutionLimit), totalSearchLimitSec(totalSearchLimitSec), localSearchLimitSec(localSearchLimitSec),
    firstSolutionStrategy(firstSolutionStrategy), localSearchStrategy(localSearchStrategy), detailedRoutes(detailedRoutes),
    names(names), distMat(distMat), capacity(capacity), demands(demands)
{}



VrpSolverThread::VrpSolverThread(
    int solutionLimit, int64 localSearchLimitSec, int64 totalSearchLimitSec,
    FirstSolutionStrategy_Value firstSolutionStrategy, LocalSearchMetaheuristic_Value localSearchStrategy, bool detailedRoutes,
    QVector<QString> names, QVector<QVector<int64>> distMat, QVector<int64> demands, int capacity,

    int vehicleCount, RoutingIndexManager::NodeIndex depot
)
:
  VrpSolverThread(solutionLimit, localSearchLimitSec, totalSearchLimitSec, firstSolutionStrategy, localSearchStrategy, detailedRoutes, names, distMat, demands, capacity)
{
    multiDepot          = false;
    this->depot         = depot;
    this->vehicleCount  = vehicleCount;
}



VrpSolverThread::VrpSolverThread(
    int solutionLimit, int64 localSearchLimitSec, int64 totalSearchLimitSec,
    FirstSolutionStrategy_Value firstSolutionStrategy, LocalSearchMetaheuristic_Value localSearchStrategy, bool detailedRoutes,
    QVector<QString> names, QVector<QVector<int64>> distMat, QVector<int64> demands, int capacity,

    QVector<RoutingIndexManager::NodeIndex> vehicleStartEnds
)
:
    VrpSolverThread(solutionLimit, localSearchLimitSec, totalSearchLimitSec, firstSolutionStrategy, localSearchStrategy, detailedRoutes, names, distMat, demands, capacity)
{
    multiDepot              = true;
    this->vehicleStartEnds  = vehicleStartEnds.toStdVector();
    vehicleCount            = vehicleStartEnds.count();
}


void VrpSolverThread::run()
{
    if ( !multiDepot )  initSingleDepotSolver();
    else                initMultiDepotSolver();
}



void VrpSolverThread::initSingleDepotSolver()
{
    RoutingIndexManager indexManager(distMat.size(), vehicleCount, depot);
    RoutingModel        routingModel(indexManager);

    solve(indexManager, routingModel);
}



void VrpSolverThread::initMultiDepotSolver()
{
    RoutingIndexManager indexManager(distMat.size(), vehicleCount, vehicleStartEnds, vehicleStartEnds); //
    RoutingModel        routingModel(indexManager);

    solve(indexManager, routingModel);
}



void VrpSolverThread::solve(RoutingIndexManager &indexManager, RoutingModel &routingModel)
{
    // distance

    const int transitCallbackIndex = routingModel.RegisterTransitCallback(
        [this, &indexManager] (int64 fromIndex, int64 toIndex) -> int64 {
            int fromNodeIndex   = indexManager.IndexToNode(fromIndex).value();
            int toNodeIndex     = indexManager.IndexToNode(toIndex).value();
            return distMat[fromNodeIndex][toNodeIndex];
        }
    );
    routingModel.SetArcCostEvaluatorOfAllVehicles(transitCallbackIndex);                        // define cost of each arc
    routingModel.AddDimension(transitCallbackIndex, 0, kint64max, true, "Distance");            // add distance



    // capacity

    if ( capacity > 0 && demands.count() > 0 )
    {
        const int demandCallbackIndex = routingModel.RegisterUnaryTransitCallback([this, &indexManager] (int64 fromIndex) -> int64 {
            return demands[indexManager.IndexToNode(fromIndex).value()];
        });

        routingModel.AddDimension(demandCallbackIndex, 0, capacity, true, "Capacity");
//        routingModel.AddDimensionWithVehicleCapacity(demandCallbackIndex, 0, {capVec}, true, "Capacity);" // for capacities per vehicle, not yet supported by UI
    }



    // parameters

    const_cast<RoutingDimension&>(routingModel.GetDimensionOrDie("Distance")).SetGlobalSpanCostCoefficient(100);// make distance predominant factor in the objective function

    RoutingSearchParameters searchParameters = DefaultRoutingSearchParameters();                                // set startegy parameters
    searchParameters.set_first_solution_strategy(firstSolutionStrategy);
    searchParameters.set_local_search_metaheuristic(localSearchStrategy);

    if ( solutionLimit == 0 )
        solutionLimit = 1;
                                                                                                                // if limitParam == 0 use defaults:
    if ( solutionLimit       > 1 ) searchParameters.set_number_of_solutions_to_collect(solutionLimit);          // default 1
    if ( totalSearchLimitSec > 0 ) searchParameters.mutable_time_limit()->set_seconds(totalSearchLimitSec);     // default kint64max
    if ( localSearchLimitSec > 0 ) searchParameters.mutable_lns_time_limit()->set_seconds(localSearchLimitSec); // default 100




    // start search

    if ( solutionLimit == 1 )
    {
        const Assignment* solution = routingModel.SolveWithParameters(searchParameters);        
        emit resultReady(1, solutionToStringList(indexManager, routingModel, solution));
    }
    else
    {
        vector<const Assignment*> solutions;
        routingModel.SolveWithParameters(searchParameters, &solutions);
        int i = 0;
        for ( const Assignment* solution : solutions )
            emit resultReady(++i, solutionToStringList(indexManager, routingModel, solution));
    }
}



QStringList VrpSolverThread::solutionToStringList(const RoutingIndexManager& indexManager, const RoutingModel& routingModel, const Assignment* solution)
{
    QStringList  routes;
    QVector<int> routeLengths;

    if ( solution == nullptr )
    {
        routes << "No route(s) found ...";
        return routes;
    }


    for ( int vehicleNr = 0; vehicleNr < vehicleCount; ++vehicleNr )
    {
        QString resultString("");

        int64 previousIndex = -1;
        int64 index         = routingModel.Start(vehicleNr);    // index of internal csp variable   !!!
        int   node;                                             // index in distance matrix         !!!
        int   routeLength   = 0;
        int   locations     = 0;

        bool    capacityGiven   = capacity > 0 && demands.count() > 0;
        int     routeLoad       = 0;
        QString loadTxt;


        while ( !routingModel.IsEnd(index) )
        {
            node = static_cast<int>(indexManager.IndexToNode(index).value());

            // if problem has pickup/deliver constraints, keep track of vehicle load
            if ( detailedRoutes && capacityGiven )
            {
                routeLoad += demands[node];
                loadTxt = QString::number(routeLoad) + "/" + QString::number(capacity);
            }

            // in depot a distance of 0m was travelled so far
            if ( previousIndex == -1 )
            {
                resultString += names[node];
                if ( detailedRoutes )
                    resultString += " (" + (capacityGiven ? loadTxt + ", " : "") + "0 m)";
            }
            // for all transits keep track of distance travelled between the 2 locations
            else
            {
                routeLength  += const_cast<RoutingModel&>(routingModel).GetArcCostForVehicle(previousIndex, index, int64{vehicleNr});
                resultString += names[node];
                if ( detailedRoutes )
                    resultString += " (" + (capacityGiven ? loadTxt + ", " : "") + QString::number(routeLength) + " m)";
            }

            previousIndex   = index;
            index           = solution->Value(routingModel.NextVar(index));
            resultString    += " -> ";

            if ( !routingModel.IsEnd(index) )
                ++locations;

        } // location loop


        // add depot end stop
        node            = static_cast<int>(indexManager.IndexToNode(index).value());
        routeLength     += const_cast<RoutingModel&>(routingModel).GetArcCostForVehicle(previousIndex != -1 ? previousIndex : index, index, int64{vehicleNr});
        resultString    += names[node];
        if ( detailedRoutes )
            resultString += " (" + (capacityGiven ? loadTxt + ", " : "") + QString::number(routeLength) + " m)";

        routes          << resultString.prepend("Route for vehicle Nr. " + QString::number(vehicleNr) + " (" + QString::number(locations) + " Locations):\n");
        routeLengths    << routeLength;

    } // vehicle loop

    routeLengths.removeAll(0);  // ignore empty routes in evaluation

    int min, max, avg, med;
    Util::getMinMaxAvgMedian(routeLengths, min, max, avg, med);

    routes << "Shortest route: " + QString::number(min) + " m";
    routes << "Longest route: " + QString::number(max) + " m";
    routes << "Avgerage route: ~" + QString::number(avg) + " m";
    routes << "Median route: ~" + QString::number(med) + " m";
    routes << "Total time: ~" + QString::number(routingModel.solver()->wall_time()) + " ms";

    return routes;
}
