/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file vrpsolverthread.h
 * @author Niklas Wahrenberg
 */

#ifndef VRPSOLVERTHREAD_H
#define VRPSOLVERTHREAD_H

#include <QtCore>
#include <QObject>
#include <QThread>

#include "ortools/constraint_solver/routing.h"
#include "ortools/constraint_solver/routing_enums.pb.h"
#include "ortools/constraint_solver/routing_index_manager.h"
#include "ortools/constraint_solver/routing_parameters.h"
#include "ortools/constraint_solver/routing_index_manager.h"

using namespace operations_research;
using namespace std;




class VrpSolverThread : public QThread
{
    Q_OBJECT

typedef _int64 int64;

private:    
    int                                     solutionLimit;
    int64                                   totalSearchLimitSec;
    int64                                   localSearchLimitSec;

    FirstSolutionStrategy_Value             firstSolutionStrategy;
    LocalSearchMetaheuristic_Value          localSearchStrategy;

    bool                                    detailedRoutes;

    QVector<QString>                        names;
    QVector<QVector<int64>>                 distMat;
    int                                     vehicleCount;
    int                                     capacity;

    QVector<int64>                          demands;

    bool                                    multiDepot;
    RoutingIndexManager::NodeIndex          depot;              // used in !multiDepot
    vector<RoutingIndexManager::NodeIndex>  vehicleStartEnds;   // used in  multiDepot



    VrpSolverThread(
        int solutionLimit, int64 localSearchLimitSec, int64 totalSearchLimitSec,
        FirstSolutionStrategy_Value firstSolutionStrategy, LocalSearchMetaheuristic_Value localSearchStrategy, bool detailedRoutes,
        QVector<QString> names, QVector<QVector<int64>> distMat, QVector<int64> demands, int capacity
    );


    QStringList solutionToStringList(const RoutingIndexManager& indexManager, const RoutingModel& routingModel, const Assignment* solution);



public:

    VrpSolverThread(
        int solutionLimit, int64 localSearchLimitSec, int64 totalSearchLimitSec,
        FirstSolutionStrategy_Value firstSolutionStrategy, LocalSearchMetaheuristic_Value localSearchStrategy, bool detailedRoutes,
        QVector<QString> names, QVector<QVector<int64>> distMat, QVector<int64> demands, int capacity,

        int vehicleCount, RoutingIndexManager::NodeIndex depot
    );



    VrpSolverThread(
        int solutionLimit, int64 localSearchLimitSec, int64 totalSearchLimitSec,
        FirstSolutionStrategy_Value firstSolutionStrategy, LocalSearchMetaheuristic_Value localSearchStrategy, bool detailedRoutes,
        QVector<QString> names, QVector<QVector<int64>> distMat, QVector<int64> demands, int capacity,

        QVector<RoutingIndexManager::NodeIndex> vehicleStartEnds
    );



    void run() override;

    void initSingleDepotSolver();
    void initMultiDepotSolver();
    void solve(RoutingIndexManager& indexManager, RoutingModel& routingModel);



signals:
    void resultReady(int solutionNr, QStringList routes);

};

#endif // VRPSOLVERTHREAD_H
