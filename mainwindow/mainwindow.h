/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file mainwindow.h
 * @author Niklas Wahrenberg
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtWidgets>

#include "util.h"

class CspTab;
class VrpTab;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    Ui::MainWindow*     ui;
    CspTab*             cspTab;
    VrpTab*             vrpTab;
    QActionGroup*       selectionGroup;

    void initActions();

public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow() override;

    void closeEvent(QCloseEvent *event)             override;
    void contextMenuEvent(QContextMenuEvent *event) override;
};

#endif // MAINWINDOW_H
