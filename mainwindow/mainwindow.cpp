/*
This file is part of the OR-Tools CSP & VRP evaluation program.

The OR-Tools CSP & VRP evaluation program is free software:
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The OR-Tools CSP & VRP evaluation program is distributed
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the OR-Tools CSP & VRP evaluation program.
If not, see <http://www.gnu.org/licenses/>.
*/

#include <QDebug>
#include <QDesktopServices>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_csptab.h"

#include "csp/csptab.h"
#include "vrp/vrptab.h"



MainWindow::MainWindow(QWidget *parent)
    :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    cspTab(new CspTab(this)),
    vrpTab(new VrpTab(this)),
    selectionGroup(new QActionGroup(this))
{
    ui->setupUi(this);

    removeToolBar(ui->toolBar);
    addToolBar(Qt::LeftToolBarArea, ui->toolBar);
    ui->toolBar->show();

    ui->mainTab->addTab(cspTab, QIcon(":/images/icons/icon_share.png"), "CSP");
    ui->mainTab->addTab(vrpTab, QIcon(":/images/icons/icon_map.png"), "VRP");

    initActions();
}



MainWindow::~MainWindow()
{
    delete ui;    
}



void MainWindow::closeEvent(QCloseEvent *event)
{
    if ( cspTab->isSolverRunning() || vrpTab->isSolverRunning() )
    {        
        if ( QMessageBox::question(this, tr("Unfinished business"),
                                   tr("A solver thread is still running, do you really want to quit?"),
                                   QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes )
            event->accept();
        else
            event->ignore();
    }
}



void MainWindow::contextMenuEvent(QContextMenuEvent *event)
{
    QMenu menu(this);
    menu.addMenu(ui->menuListSelectionMode);
    menu.addMenu(ui->menuExportImport);
    menu.addMenu(ui->menuGenerateProblem);
    menu.addAction(ui->actionToggleToolbar);
    menu.exec(event->globalPos());
}



void MainWindow::initActions()
{
   connect(ui->actionQuit, &QAction::triggered, this, &QMainWindow::close);


   // signal hand over to internal convenience action
   connect(ui->actionToggleToolbar, &QAction::triggered, ui->toolBar->toggleViewAction(), &QAction::trigger);


   selectionGroup->addAction(ui->actionToggleClick);
   selectionGroup->addAction(ui->actionExtendedMultiSelection);
   connect(ui->actionToggleClick, &QAction::toggled, [this](bool checked){
        if ( checked )
        {
            cspTab->setListSelectionMode(QAbstractItemView::MultiSelection);
            vrpTab->setListSelectionMode(QAbstractItemView::MultiSelection);
        }
    });
   connect(ui->actionExtendedMultiSelection, &QAction::toggled, [this](bool checked){
        if (checked )
        {
            cspTab->setListSelectionMode(QAbstractItemView::ExtendedSelection);
            vrpTab->setListSelectionMode(QAbstractItemView::ExtendedSelection);
        }
    });


   connect(ui->actionExportProblem, &QAction::triggered, [this] () {
       if      ( ui->mainTab->currentWidget() == cspTab ) cspTab->handleActionExport();
       else if ( ui->mainTab->currentWidget() == vrpTab ) vrpTab->handleActionExport();
   });


   connect(ui->actionImportProblem, &QAction::triggered, [this]() {
        if      ( ui->mainTab->currentWidget() == cspTab ) cspTab->handleActionImport();
        else if ( ui->mainTab->currentWidget() == vrpTab ) vrpTab->handleActionImport();
   });


   connect(ui->actionHSB, &QAction::triggered, [](){
        QDesktopServices::openUrl(QUrl(tr("https://www.hs-bremen.de/internet/en/index.html")));
    });


   connect(ui->actionORTools, &QAction::triggered, [](){
        QDesktopServices::openUrl(QUrl(tr("https://developers.google.com/optimization/")));
    });


   connect(ui->actionInfo, &QAction::triggered, [this](){
        QMessageBox::information(this, STRINGS::MSG_INFO, tr("Created by Niklas Wahrenberg, 5001930<br>"
                                                    "as part of his B.Sc. Thesis in Summer Term 2019<br>"
                                                    "<br>"
                                                    "Supervised by"
                                                        "<ul>"
                                                            "<li>Prof. Dr. rer. nat. Jörg Mielebacher</li>"
                                                            "<li>Prof. Dr.-Ing. Uwe Meyer</li>"
                                                        "</ul>"
                                                    "<br>"
                                                    "Credits"
                                                        "<ul>"
                                                            "<li><a href=\"https://www.qt.io/\">Qt</a></li>"
                                                            "<li><a href=\"https://developers.google.com/optimization/\">OR-Tools</a></li>"
                                                            "<li><a href=\"https://www.iconfinder.com/\">Iconfinder.com</a></li>"
                                                        "</ul>"
                                                    "<br>"
                                                    "Licensed under <a href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\">GPLv3</a>"
                                                    "<br><br>"
                                                    "Source is available under<br><a href=\"https://gitlab.com/niklasw_bsc2019/ortools_eval\">https://gitlab.com/niklasw_bsc2019/ortools_eval</a>"
                                              )
                                 );
    });

   connect(ui->actionnQueens,           &QAction::triggered, cspTab, &CspTab::generateNQueensProblem);
   connect(ui->actionMagicSquare,       &QAction::triggered, cspTab, &CspTab::generateMagicSquareProblem);
   connect(ui->actionLegsAndHeads,      &QAction::triggered, cspTab, &CspTab::generateLegsAndHeadsProblem);
   connect(ui->actionMapColoring,       &QAction::triggered, cspTab, &CspTab::generateMapColoringProblem);

   connect(ui->actionLocations,         &QAction::triggered, vrpTab, &VrpTab::generateLocations);
   connect(ui->actionLocationsDemands,  &QAction::triggered, vrpTab, &VrpTab::generateLocationsAndDemands);
}
