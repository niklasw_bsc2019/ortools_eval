#-------------------------------------------------
#
# Project created by QtCreator 2019-06-01T16:36:03
#
#-------------------------------------------------

QT       += core gui widgets webenginewidgets

TARGET = solver
TEMPLATE = app

DEFINES += NOMINMAX
DEFINES += _WINSOCKAPI_
#DEFINES += _ITERATOR_DEBUG_LEVEL = 2
#DEFINES += RuntimeLibrary = MDd_DynamicDebug

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow/mainwindow.cpp \
        csp/entities/variable.cpp \
        util.cpp \
        csp/csptab.cpp \
        csp/solver/cspsolver.cpp \
        vrp/vrptab.cpp \
        vrp/solver/vrpsolverthread.cpp

HEADERS += \
        mainwindow/mainwindow.h \
        csp/csptab.h \
        vrp/vrptab.h \
        util.h \
        csp/entities/variable.h \
        csp/entities/abstractconstraint.h \
        csp/entities/constraintmodproddiv.h \
        csp/entities/constraintminmax.h \
        csp/entities/constraintalldiff.h \
        csp/entities/constraintminmaxeq.h \
        csp/entities/constraintsum.h \
        csp/entities/constraintsimplerelation.h \
        csp/entities/constraintscalarprod.h \
        csp/datamodel/cspentitylistitem.h \
        csp/datamodel/cspentitylistmodel.h \
        csp/solver/cspsolverthread.h \
        vrp/solver/vrpsolverthread.h \
        vrp/regexpdelegate.h


FORMS += \
        mainwindow/mainwindow.ui \
        csp/csptab.ui \
        vrp/vrptab.ui

LIBS += or-tools_VisualStudio2017-64bit_v7.0.6546/ortools.lib
INCLUDEPATH += or-tools_VisualStudio2017-64bit_v7.0.6546/include
#DEPENDPATH += or-tools_VisualStudio2017-64bit_v7.0.6546/include

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
