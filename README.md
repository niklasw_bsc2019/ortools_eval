# [OR-Tools](https://developers.google.com/optimization/) CSP & VRP evaluation program

[![](https://doc.qt.io/qt-5.12/images/used-in-examples/animation/easing/images/qt-logo.png)](https://www.qt.io/)

[![](https://developers.google.com/optimization/images/GoogleAI_logo_small.png)](https://developers.google.com/optimization/)

----

### Features

+ Creation of CSPs & VRPs in a GUI with a few clicks
+ Generation of common sample problems
    + n-Queens
    + Magic Square
    + Heads & Legs
    + Map Coloring
    + VRP (with *x* locations and *y* vehicles)
    + CVRP (with *x* locations, *y* vehicles, *z* capacity per vehicle)
+ Display found solutions and the time to find them
+ Search parameters can be specified
    + CSP    
        + Any / all solution(s)
    + VRP
        + Best k solutions   
        + Initial route strategy
        + Local search startegy
        + Time limits
+ Import / export of problems ([JSON](https://www.json.org/))

----

### TODOs
+ Code comments
+ API generation with [Doxygen](http://www.doxygen.nl/)
+ Create expressive tooltips
+ Write manual / usage guide
+ Unit-Testing, UI Tests
+ Add support for evaluation of other OR-Tools modules
+ Add support for evaluation of other OR libs

----

### Credits
+ [Qt](https://www.qt.io/)
+ [OR-Tools](https://developers.google.com/optimization/)
+ [Iconfinder](https://www.iconfinder.com/)

----

### License
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

----

### Author
Niklas Wahrenberg

niklas[.]wahrenberg[at]gmx[.]de

nwahrenberg[at]gmail[.]com

Created in summer term 2019 for my bachelor thesis at  [HSB (City University of Applied Sciences Bremen)](https://www.hs-bremen.de/internet/en/index.html)

----

### Download
Release v0.1: [ortools_eval.zip (Win, 64bit)](https://gitlab.com/niklasw_bsc2019/ortools_eval/blob/master/ortools_eval_v0.1.zip)

----

### Screenshots
[Magic square & small VRP (on imgur.com)](https://imgur.com/a/39iMS6V)

----

### Evaluation results
[eval.xlsx (on Dropbox)](https://www.dropbox.com/s/liucox7n8sjzgt6/eval.xlsx?dl=0)